package ${package.Service};

import ${package.Entity}.${entity};
import com.yinxing.framework.mybatis.TemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
public class ${table.serviceName} extends TemplateService<${entity}> {

}