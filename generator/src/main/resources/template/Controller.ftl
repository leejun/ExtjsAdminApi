package ${package.Controller};

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${package.Entity}.${entity};
import ${package.Service}.${table.serviceName};
import com.yinxing.framework.domain.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/${entity}Controller")
public class ${entity}Controller {

    @Autowired
    private ${table.serviceName} service;

    /**
     * 分页查询
     * @param page 分页模型(url?page=1&limit=20)
     * @param params 查询参数 url?page=1&limit=20&key1=value1&key2=value2
     */
    @GetMapping("/selectPage")
    public R selectPage(Page<${entity}> page, @RequestParam Map<String,String> params) {
        QueryWrapper<${entity}> qw = new QueryWrapper<>();
        IPage<${entity}> iPage = service.selectPage(page, qw);
        return R.ok(iPage);
    }

    /**
     * 主键查询
     * @param id 主键
     */
    @GetMapping("/selectById")
    public R selectById(@RequestParam Long id) {
        ${entity} record = service.selectById(id);
        return R.ok(record);
    }

    /**
     * 主键删除
     * @param id 主键
     */
    @PostMapping("/deleteById")
    public R deleteById(@RequestParam Long id) {
        service.deleteById(id);
        return R.ok();
    }

    /**
     * 主键批量删除
     * @param ids 主键
    */
    @PostMapping("/deleteBatchById")
    public R deleteBatchById(@RequestParam Long[] ids) {
        service.deleteBatchById(ids);
        return R.ok();
    }

    /**
     * 新增
     * @param record 实体类
     */
    @PostMapping("/insert")
    public R insert(${entity} record) {
        service.insert(record);
        return R.ok();
    }

    /**
     * 更新
     * @param record 实体类
     */
    @PostMapping("/update")
    public R update(${entity} record) {
        service.updateById(record);
        return R.ok();
    }

}
