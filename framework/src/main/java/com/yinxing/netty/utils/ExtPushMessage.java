package com.yinxing.netty.utils;

import java.util.LinkedHashMap;

public class ExtPushMessage extends LinkedHashMap<String, Object> {

    public ExtPushMessage(MessageType type) {
        put("type", type);
    }

    public enum MessageType {
        SYS_MESSAGE,
        SYS_INFO,
        OFF_LINE,
        SYS_LOG
    }
}
