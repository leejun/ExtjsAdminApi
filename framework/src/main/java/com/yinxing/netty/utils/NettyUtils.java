package com.yinxing.netty.utils;

import io.netty.channel.Channel;
import java.net.InetSocketAddress;

public final class NettyUtils {

    /**
     * 获取远程服务器地址
     */
    public static String getRemoteHost(Channel channel) {
        InetSocketAddress ipSocket = (InetSocketAddress) channel.remoteAddress();
        return ipSocket.getAddress().getHostAddress();
    }

    /**
     * 获取远程服务器端口
     */
    public static int getRemotePort(Channel channel) {
        InetSocketAddress ipSocket = (InetSocketAddress) channel.remoteAddress();
        return ipSocket.getPort();
    }

    /**
     * 获取远程服务器 地址:端口
     */
    public static String getRemoteHostPort(Channel channel) {
        InetSocketAddress ipSocket = (InetSocketAddress) channel.remoteAddress();
        return ipSocket.getAddress().getHostAddress() + ":" + ipSocket.getPort();
    }
}
