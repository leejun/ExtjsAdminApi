package com.yinxing.webapi.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yinxing.framework.constant.SysConstant;
import com.yinxing.framework.convert.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.nio.charset.Charset;
import java.util.List;

@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 添加静态资源映射
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/favicon.ico").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    /**
     * 添加参数类型转换器
     */
    @Override
    protected void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new PageParamConvert());
        argumentResolvers.add(new JsonParamConvert(objectMapper));
    }

    /**
     * 添加参数类型转换器
     */
    @Override
    protected void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToDateConvert());
        registry.addConverterFactory(new StringToNumberConvert());
        registry.addConverter(new StringToLocalDateTimeConverter());
        registry.addConverter(new StringToLocalDateConvert());
    }

    /**
     * 添加返回类型转换器
     */
    @Override
    protected void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new JsonHttpMessageConvert(objectMapper));
        converters.add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
        converters.add(new ByteArrayHttpMessageConverter());
    }

    /**
     * 设置允许跨域
     */
    @Override
    protected void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
            //允许跨域的头信息
            .allowedHeaders("*")
            //允许跨域的Method
            .allowedMethods("*")
            //Access-Control-Max-Age 预检请求结果的缓存时间(秒)
            .maxAge(1800)
            //暴露给客户端的Header,客户端JS可以访问.
            .exposedHeaders(SysConstant.JWT_TOKEN_HTTP_HEADER
                    + "," + SysConstant.AUTH_TOKEN_HTTP_HEADER)
            //允许跨域的来源
            .allowedOrigins("*");
    }

}
