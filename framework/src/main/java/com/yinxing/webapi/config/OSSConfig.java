package com.yinxing.webapi.config;

import com.yinxing.framework.utils.OSSUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = "yinxing.config.oss.enable", havingValue = "true")
public class OSSConfig {

	@Bean
	public OSSUtils ossUtils() {
		return new OSSUtils();
	}
}

