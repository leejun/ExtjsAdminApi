package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统部门表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dept")
public class SysDept implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId("Id_")
    private Long Id_;

    /**
     * 部门名称
     */
    private String DeptName_;

    /**
     * 部门编码
     */
    private String DeptCode_;

    /**
     * 父节点ID
     */
    private Long ParentId_;

    /**
     * 是否展开
     */
    private Boolean Expanded_;

    /**
     * 顺序
     */
    private Integer Index_;

    /**
     * 是否叶子节点
     */
    private Boolean Leaf_;

    /**
     * 是否停用
     */
    private Boolean Stop_;

    /**
     * 创建时间
     */
    private LocalDateTime CreateTime_;

}
