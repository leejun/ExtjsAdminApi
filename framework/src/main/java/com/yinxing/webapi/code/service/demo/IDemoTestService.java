//package com.yinxing.webapi.code.service.demo;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.yinxing.framework.mybatis.TemplateService;
//import com.yinxing.webapi.code.entity.demo.DemoTest;
//import com.yinxing.webapi.code.model.demo.req.QueryParams;
//import com.yinxing.webapi.code.model.demo.res.DemoTestRes;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.beanutils.BeanUtils;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//@Slf4j
//@Service
//@Transactional
//public class IDemoTestService extends TemplateService<DemoTest> {
//
//    public IPage<DemoTestRes> selectPage(QueryParams params) {
//        IPage<DemoTest> page = selectPage(new Page<>(params.getPage(), params.getLimit()), new QueryWrapper<>());
//        return page.convert(demoTest -> convert(demoTest));
//    }
//
//    private DemoTestRes convert(DemoTest src) {
//        DemoTestRes dist = new DemoTestRes();
//        try {
//            BeanUtils.copyProperties(dist, src);
//        } catch (Exception e) {
//        }
//        return dist;
//    }
//}