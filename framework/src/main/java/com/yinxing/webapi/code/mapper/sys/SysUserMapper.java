package com.yinxing.webapi.code.mapper.sys;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.yinxing.webapi.code.entity.sys.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yinxing
 * @since 2022-01-18
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    IPage<SysUser> selectUserList(IPage<SysUser> page, @Param(Constants.WRAPPER) Wrapper<SysUser> queryWrapper);

    IPage<SysUser> selectUserListByRoleId(IPage<SysUser> page, @Param("params") Map<String, Object> params);
}
