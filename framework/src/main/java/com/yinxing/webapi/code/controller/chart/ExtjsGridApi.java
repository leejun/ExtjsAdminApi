package com.yinxing.webapi.code.controller.chart;

import com.yinxing.framework.annotation.Json;
import com.yinxing.framework.domain.R;
import com.yinxing.framework.utils.LongIdUtils;
import com.yinxing.framework.utils.RandomStringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.*;

@Slf4j
@RestController
@RequestMapping("/ExtjsGridApi")
public class ExtjsGridApi {

    /**
     * 为单元格组件表格提供数据
     */
    @GetMapping("/cellWidgetDataList")
    public R cellWidgetDataList() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < 25; i++) {
            Map<String, Object> data = new LinkedHashMap<>();
            data.put("id", LongIdUtils.DEFAULT.nextId());
            data.put("name", RandomStringUtils.getRandomString(10));
            data.put("progress", generateIntValue(0, 100) / 100f); //返回俩位小数: 0.xx
            data.put("mode", i % 2 > 0 ? "Local" : "Remote");
            data.put("sequence1", generateSequence(20, -10, 10));
            data.put("sequence2", generateSequence(15, -10, 10));
            data.put("sequence3", generateSequence(20, -10, 10));
            data.put("sequence4", new int[]{150, generateIntValue(100, 280), 100, 200, 300});
            data.put("sequence5", generateSequence(4, 10, 20));
            data.put("sequence6", generateSequence(20, -10, 10));
            data.put("sequence7", generateSequence(20, -1, 1));
            list.add(data);
        }
        return R.ok(list);
    }

    @PostMapping("/cellWidgetDataUpdate")
    public R cellWidgetDataUpdate(@Json List<Map<String, Object>> records) {
        records.forEach(rec -> System.out.println(rec.toString()));
        return R.ok();
    }

    /**
     * 为表格行编辑提供数据
     */
    @GetMapping("/selectRowEditingList")
    public R selectRowEditingList() {
        List<Map<String, Object>> list = new ArrayList<>();
        String[] emails = new String[]{
                "y303h0ebusk@thrubay.com", "nbmmuuswne@iffymedia.com",
                "jfz89cnbtmq@payspun.com","l2iqq3desth@linshiyouxiang.net",
                "jfz89cnbtmq@payspun.com", "vizyp4lce@linshiyouxiang.net",
                "ik3xp9jpklk@linshiyouxiang.net", "9m6l7245jzs@payspun.com",
                "lowchfmzy7@linshiyouxiang.net", "bj7ks3gia4m@linshiyouxiang.net",
                "y303h0ebusk@thrubay.com", "nbmmuuswne@iffymedia.com",
                "jfz89cnbtmq@payspun.com","l2iqq3desth@linshiyouxiang.net",
                "jfz89cnbtmq@payspun.com", "vizyp4lce@linshiyouxiang.net",
                "ik3xp9jpklk@linshiyouxiang.net", "9m6l7245jzs@payspun.com",
                "lowchfmzy7@linshiyouxiang.net", "bj7ks3gia4m@linshiyouxiang.net",
                "8typ0utq0am@linshiyouxiang.net", "9i2nn162d54@iffymedia.com",
                "z30aor5g9x@iffymedia.com", "irpqinqyaw@iubridge.com",
                "5q69gibv7ip@temporary-mail.net"
        };
        for (int i = 0; i < 25; i++) {
            Map<String, Object> data = new LinkedHashMap<>();
            data.put("id", LongIdUtils.DEFAULT.nextId());
            data.put("name", RandomStringUtils.getRandomString(20));
            data.put("email", emails[i]);
            data.put("phone", RandomStringUtils.getRandomNumberString(11));
            data.put("active", i % 2 > 0 ? true : false);
            data.put("joinDate", generateLocalDate());
            data.put("salary", generateIntValue(5000,30000));
            list.add(data);
        }
        return R.ok(list);
    }

    @PostMapping("/rowEditingUpdate")
    public R rowEditingUpdate(@Json Map<String, Object> records) {
        System.out.println(records.toString());
        return R.ok();
    }

    /**
     * 生成随机数数组
     * @param count 数组长度
     * @param min 最小值
     * @param max 最大值
     */
    private int[] generateSequence(int count, int min, int max) {
        int[] res = new int[count];
        for (int i = 0; i < count; i++) {
            res[i] = generateIntValue(min, max);
        }
        return res;
    }

    /**
     * 生成一个范围内的随机数
     */
    public int generateIntValue(int min, int max) {
        return min + (int) (Math.random()*(max - min +1));
    }

    /**
     * 生成随机日期
     */
    public LocalDate generateLocalDate() {
        Random random = new Random();
        int minDay = (int) LocalDate.of(2000, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(2022, 3, 1).toEpochDay();
        long randomDay = minDay + random.nextInt(maxDay - minDay);
        LocalDate randomBirthDate = LocalDate.ofEpochDay(randomDay);
        return randomBirthDate;
    }
}
