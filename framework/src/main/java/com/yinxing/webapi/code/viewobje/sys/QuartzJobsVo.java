package com.yinxing.webapi.code.viewobje.sys;

import lombok.Data;

import java.util.Date;

@Data
public class QuartzJobsVo {
    private String jobName;
    private String jobGroupName;
    private String jobDescription;
    private String jobClassName;
    private String triggerName;
    private String triggerGroupName;
    private String cronExpression;
    private String triggerState;
    private Date startTime;
    private Date nextFireTime;
    private Date previousFireTime;
}
