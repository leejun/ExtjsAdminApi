package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 系统字典表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dic")
public class SysDic implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId("Id_")
    private Long Id_;

    /**
     * 字典类型ID
     */
    private Long SysDicTypeId_;

    /**
     * 字典类型编码
     */
    private String SysDicTypeCode_;

    /**
     * 字典名称
     */
    private String Name_;

    /**
     * 字典编码
     */
    private String Code_;

    /**
     * 字典顺序
     */
    private Integer Index_;

    /**
     * 父字典ID
     */
    private Long ParentId_;

    /**
     * 是否叶子节点
     */
    private Boolean Leaf_;

    /**
     * 是否展开
     */
    private Boolean Expanded_;

    /**
     * 是否停用
     */
    private Boolean Stop_;

}
