package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;


/**
 * <p>
 * 
 * </p>
 *
 * @author yinxing
 * @since 2022-02-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_generator")
public class SysGenerator implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键PK
     */
    @TableId("Id_")
    private Long Id_;

    /**
     * 字符串1
     */
    private String StringVal1_;

    /**
     * 字符串2
     */
    private String StringVal2_;

    /**
     * 字典(性别)ID
     */
    private Long SysDicSexId_;

    /**
     * 字典(城市)ID
     */
    private Long SysDicCityId_;

    /**
     * 数值1
     */
    private Integer IntVal1_;

    /**
     * 数值2
     */
    private Integer IntVal2_;

    /**
     * 日期类型
     */
    private LocalDate DateVal_;

    /**
     * 布尔类型
     */
    private Boolean BooleanVal_;


}
