package com.yinxing.webapi.code.service.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.google.common.base.CaseFormat;
import com.google.common.base.Predicate;
import com.yinxing.framework.mybatis.TemplateService;
import com.yinxing.framework.utils.LongIdUtils;
import com.yinxing.webapi.code.entity.sys.SysDictype;
import com.yinxing.webapi.code.entity.sys.SysTable;
import com.yinxing.webapi.code.entity.sys.SysTableColumn;
import com.yinxing.webapi.code.mapper.sys.SysTableMapper;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static freemarker.template.Configuration.VERSION_2_3_28;

@Slf4j
@Service
@Transactional
public class ISysTableService extends TemplateService<SysTable> {

    @Autowired
    private ISysTableColumnService sysTableColumnService;

    @Autowired
    private ISysDictypeService sysDictypeService;

    private SysTableMapper getMapper() {
        return (SysTableMapper) baseMapper;
    }

    /**
     * 把数据库中的表信息，同步到我们自己的表中
     */
    public void syncTableList() {
        List<Map<String, Object>> list = getMapper().selectTableList();
        for (Map<String, Object> m: list) {
            String tableName = m.get("TABLE_NAME").toString();
            SysTable table = selectByTableName(tableName);
            if(table == null) {
                table = new SysTable();
                table.setId_(LongIdUtils.DEFAULT.nextId());
                table.setTableName_(tableName);
                table.setTableComment_(m.get("TABLE_COMMENT").toString());
                Object createTime =  m.get("CREATE_TIME");
                if(createTime instanceof Timestamp) {
                    table.setCreateTime_(((Timestamp) createTime).toLocalDateTime());
                }
                if(createTime instanceof LocalDateTime) {
                    table.setCreateTime_((LocalDateTime) createTime);
                }

                table.setPackage_(tableName.replace("_", "."));
                table.setLayoutType_("grid");
                String modelName = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, tableName);
                table.setModelName_(modelName);
                table.setSyncTime_(LocalDateTime.now());
                table.setIsPagination_(true);
                table.setIsRowNum_(false);
                //multi(多选) | single(单选) | none(不选)
                table.setSelectModel_("single");
                table.setSysDicTypeId_(0L);
                table.setSysDicTypeCode_("");
                this.getMapper().insert(table);
            }
        }
    }

    /**
     * 根据表名查询数据
     * @param tableName 数据库表名称
     */
    public SysTable selectByTableName(String tableName) {
        QueryWrapper<SysTable> qw = new QueryWrapper<>();
        qw.eq("TableName_", tableName);
        return baseMapper.selectOne(qw);
    }

    /**
     * 根据ID进行更新
     * @param entity
     */
    public boolean updateById(SysTable entity) {
        //如果sys_dictype表id不为空 则更新code字段
        if(entity.getSysDicTypeId_() != null) {
            SysDictype dictype = sysDictypeService.selectById(entity.getSysDicTypeId_());
            if(dictype != null) {
                entity.setSysDicTypeCode_(dictype.getCode_());
            } else {
                entity.setSysDicTypeCode_("");
            }
        }
        return SqlHelper.retBool(baseMapper.updateById(entity));
    }

    /**
     * 删除自己和sys_table_column
     * @param id PK
     */
    public void deleteMeAndColumns(long id) {
        //删除子表数据
        sysTableColumnService.deleteBySysTableId(id);
        //删除自己
        super.deleteById(id);
    }

    /**
     * 生成代码
     * @param tableId sys_table表id
     */
    public byte[] downloadCode(Long tableId) throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);

        //Freemaker初始化
        Configuration cfg = new Configuration(VERSION_2_3_28);
        cfg.setClassForTemplateLoading(ISysTableService.class, "/");
        DefaultObjectWrapper objectWrapper = new DefaultObjectWrapper(VERSION_2_3_28);
        objectWrapper.setExposeFields(true);
        cfg.setObjectWrapper(objectWrapper);

        //数据表基本信息
        SysTable sysTable = selectById(tableId);
        //数据列信息集合
        List<SysTableColumn> columnList = sysTableColumnService.selectListByTableId(tableId);
        //过滤出放在ActionPanel.js中的列
        List<SysTableColumn> actionList = columnList.stream()
                .filter((Predicate<SysTableColumn>) input -> input.getIsQueryAction_() == Boolean.TRUE)
                .collect(Collectors.toList());

        //过滤出放在InsertFormWin.js当中的列
        List<SysTableColumn> insertList = columnList.stream()
                .filter((Predicate<SysTableColumn>) input -> input.getIsInsert_() == Boolean.TRUE)
                .collect(Collectors.toList());

        //过滤出放在EditFormWin.js当中的列
        List<SysTableColumn> editList = columnList.stream()
                .filter((Predicate<SysTableColumn>) input -> input.getIsUpdate_() == Boolean.TRUE)
                .collect(Collectors.toList());

        //过滤出放在DetailFormWin.js当中的列
        List<SysTableColumn> detailList = columnList.stream()
                .filter((Predicate<SysTableColumn>) input -> input.getIsDetail_() == Boolean.TRUE)
                .collect(Collectors.toList());

        //过滤出放在SearchFormWin.js当中的列
        List<SysTableColumn> searchlList = columnList.stream()
                .filter((Predicate<SysTableColumn>) input -> input.getIsQuery_() == Boolean.TRUE)
                .collect(Collectors.toList());

        Map<String, Object> rootData = new HashMap<>(2);
        //封装到map中 传递给freemaker填充模板
        rootData.put("sysTable", sysTable);
        rootData.put("columnList", columnList);
        rootData.put("actionList", actionList);
        rootData.put("insertList", insertList);
        rootData.put("editList", editList);
        rootData.put("detailList", detailList);
        rootData.put("searchlList", searchlList);

        //sys.table 替换为 sys_table (作为ext视图xtype的前缀 如: xtype: 'sys_user_mainview')
        rootData.put("xtypePrefix", sysTable.getPackage_().replace(".", "_"));

        //生成ext-model文件  文件名如: SysUserModel.js
        templateProcess(zip, sysTable.getModelName_()+"Model.js", cfg, "template/ExtModel.ftl", rootData);

        //生成ext-store文件  文件名如: SysUserStore.js
        templateProcess(zip, sysTable.getModelName_()+"Store.js", cfg, "template/ExtStore.ftl", rootData);

        //生成ext-mainview文件 根据布局类型选择模板
        String mainviewTpl = "template/MainView.ftl";  //单表增删改模板
        if("treegrid".equals(sysTable.getLayoutType_())) {
            mainviewTpl = "template/MainViewTree.ftl"; //左树增删改模板
        }
        templateProcess(zip, "MainView.js", cfg, mainviewTpl, rootData);

        //生成ext-gridlist文件
        templateProcess(zip, "GridList.js", cfg, "template/GridList.ftl", rootData);

        //生成ActionPanel.js文件
        templateProcess(zip, "ActionPanel.js", cfg, "template/ActionPanel.ftl", rootData);

        //生成InsertFormWinController.js文件
        templateProcess(zip, "InsertFormWinController.js", cfg, "template/InsertFormWinController.ftl", rootData);
        //生成InsertFormWin.js文件
        templateProcess(zip, "InsertFormWin.js", cfg, "template/InsertFormWin.ftl", rootData);

        //生成EditFormWinController.js文件
        templateProcess(zip, "EditFormWinController.js", cfg, "template/EditFormWinController.ftl", rootData);
        //生成EditFormWin.js文件
        templateProcess(zip, "EditFormWin.js", cfg, "template/EditFormWin.ftl", rootData);

        //生成DetailFormWinController.js文件
        templateProcess(zip, "DetailFormWinController.js", cfg, "template/DetailFormWinController.ftl", rootData);
        //生成DetailFormWin.js文件
        templateProcess(zip, "DetailFormWin.js", cfg, "template/DetailFormWin.ftl", rootData);

        //生成SearchFormWinController.js文件
        templateProcess(zip, "SearchFormWinController.js", cfg, "template/SearchFormWinController.ftl", rootData);
        //生成SearchFormWin.js文件
        templateProcess(zip, "SearchFormWin.js", cfg, "template/SearchFormWin.ftl", rootData);

        //生成MainViewController.js文件
        templateProcess(zip, "MainViewController.js", cfg, "template/MainViewController.ftl", rootData);

        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }

    private void templateProcess(ZipOutputStream zipOutputStream, String zipFileName,
                                   Configuration cfg, String tplPath, Map<String,Object> rootData) throws Exception{
        Template template = cfg.getTemplate(tplPath);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(out, "utf-8");
        template.process(rootData, writer);

        //把byte[]写入zip流中
        zipOutputStream.putNextEntry(new ZipEntry(zipFileName));
        zipOutputStream.write(out.toByteArray());
        zipOutputStream.flush();
        zipOutputStream.closeEntry();
    }

}