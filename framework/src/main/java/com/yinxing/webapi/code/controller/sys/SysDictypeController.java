package com.yinxing.webapi.code.controller.sys;

import com.yinxing.framework.annotation.Json;
import com.yinxing.framework.domain.R;
import com.yinxing.webapi.code.service.sys.ISysDictypeService;
import com.yinxing.webapi.code.viewobje.sys.DicTypeTreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/SysDictypeController")
public class SysDictypeController {

    @Autowired
    private ISysDictypeService service;

    /**
     * 查询列表树
     */
    @GetMapping("/selectTreeList")
    public R selectTreeList() {
        List<DicTypeTreeNode> dictypeList = service.selectTreeList();
        return R.ok(dictypeList);
    }

    /**
     * 查询列表(叶子节点&&未停用)
     */
    @GetMapping("/selectNoRootList")
    public R selectNoRootList() {
        List<DicTypeTreeNode> dictypeList = service.selectNoRootList();
        return R.ok(dictypeList);
    }

    /**
     * 主键删除
     * @param id 主键
     */
    @PostMapping("/deleteById")
    public R deleteById(String id) {
        service.deleteById(id);
        return R.ok();
    }

    /**
     * 批量新增或修改
     * @param records 数据JSON集合
     */
    @PostMapping("/insertOrUpdateBatch")
    public R insertOrUpdateBatch(@Json List<DicTypeTreeNode> records) {
        service.insertOrUpdateBatch(records);
        return R.ok();
    }

    /**
     * 批量删除
     * @param records 数据JSON集合
     */
    @PostMapping("/deleteBatch")
    public R deleteBatch(@Json List<DicTypeTreeNode> records) {
        service.deleteBatch(records);
        return R.ok();
    }

    /**
     * 启用|禁用
     * @param id 主键ID
     */
    @PostMapping("/stopById")
    public R stopById(long id) {
        service.stopById(id);
        return R.ok();
    }

}
