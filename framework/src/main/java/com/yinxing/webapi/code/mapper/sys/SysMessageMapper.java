package com.yinxing.webapi.code.mapper.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.yinxing.webapi.code.entity.sys.SysMessage;
import com.yinxing.webapi.code.entity.sys.SysMessageQuery;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yinxing
 * @since 2022-02-02
 */
public interface SysMessageMapper extends BaseMapper<SysMessage> {

    IPage<SysMessageQuery> selectMyPageList(IPage<SysMessage> page,
                                            @Param(Constants.WRAPPER) QueryWrapper<SysMessage> queryWrapper);
}
