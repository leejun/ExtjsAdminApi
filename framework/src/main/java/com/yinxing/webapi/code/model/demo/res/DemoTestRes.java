package com.yinxing.webapi.code.model.demo.res;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class DemoTestRes {

    @ApiModelProperty(value = "主键ID")
    private Long Id_;

    @ApiModelProperty(value = "用户名称")
    private String Username_;

    @ApiModelProperty(value = "电话号码")
    private String TelNumber_;

    @ApiModelProperty(value = "创建日期")
    private LocalDateTime CreateTime_;

    @ApiModelProperty(value = "用户生日")
    private LocalDate Birthday_;

    @ApiModelProperty(value = "用户薪水")
    private Double Salary_;

    @ApiModelProperty(value = "是否在职")
    private Boolean OnJob_;
}
