package com.yinxing.webapi.code.viewobje.sys;

import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@ToString
public class SysUserVo {

    private Long Id_;

    /**
     * 姓名
     */
    private String Name_;

    /**
     * 登录名
     */
    private String LoginName_;

    /**
     * 邮箱
     */
    private String Email_;

    /**
     * 电话
     */
    private String Phone_;

    /**
     * 部门ID
     */
    private Long SysDeptId_;

    /**
     * 部门名称
     */
    private String SysDeptName_;

    /**
     * 部门编码
     */
    private String SysDeptCode_;

    /**
     * 禁用
     */
    private Boolean Stop_;

    /**
     * 显示顺序
     */
    private Integer Index_;

    /**
     * 登录密码: MD5(明文+盐)
     */
    private String Password_;

    /**
     * 盐值(32随机字符串)
     */
    private String Salt_;

    /**
     * 创建时间
     */
    private LocalDateTime CreateTime_;

    /**
     * 头像路径
     */
    private String PictureUrl_;

    /**
     * 角色集合
     */
    private Long[] Roles_;

    /**
     * 绑定角色
     */
    private Boolean RoleBind_;
}
