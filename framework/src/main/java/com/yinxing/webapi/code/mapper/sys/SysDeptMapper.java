package com.yinxing.webapi.code.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxing.webapi.code.entity.sys.SysDept;

/**
 * <p>
 * 系统部门表 Mapper 接口
 * </p>
 *
 * @author yinxing
 * @since 2022-01-18
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
