package com.yinxing.webapi.code.model.demo.req;

import com.yinxing.framework.mybatis.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("查询条件")
public class QueryParams extends PageParam {

    @ApiModelProperty(value = "用户名称")
    @NotBlank(message = "账号不能为空")
    @Length(min = 6, max = 20, message = "账户长度必须6-20位")
    private String username;

    @ApiModelProperty(value = "手机号码")
    @NotBlank(message = "手机号码不能为空")
    @Length(min = 11, max = 11, message = "手机号码长度必须为11位")
    private String telnumber;
}
