package com.yinxing.webapi.code.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxing.webapi.code.entity.sys.SysRolepermission;

/**
 * <p>
 * 角色权限关系表 Mapper 接口
 * </p>
 *
 * @author yinxing
 * @since 2022-01-30
 */
public interface SysRolepermissionMapper extends BaseMapper<SysRolepermission> {

}
