package com.yinxing.webapi.code.viewobje.aspose;

import com.yinxing.framework.utils.LongIdUtils;

public class Individual {
    private long id;
    private String name;
    private int age;
    private String email;
    private String address;
    private String phone;
    private String weight;
    private String sex;
    private String cardno;

    public Individual(String name, int age, String email, String address, String phone, String weight, String sex, String cardno) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.weight = weight;
        this.sex = sex;
        this.cardno = cardno;
        this.id = LongIdUtils.DEFAULT.nextId();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getWeight() {
        return weight;
    }

    public String getSex() {
        return sex;
    }

    public String getCardno() {
        return cardno;
    }

}
