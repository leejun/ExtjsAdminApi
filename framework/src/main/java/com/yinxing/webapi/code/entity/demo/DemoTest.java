package com.yinxing.webapi.code.entity.demo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author yinxing
 * @since 2022-05-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("demo_test")
public class DemoTest implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("Id_")
    private Long Id_;

    private String Username_;

    private String TelNumber_;

    private LocalDateTime CreateTime_;

    private LocalDate Birthday_;

    private Double Salary_;

    private Boolean OnJob_;

}
