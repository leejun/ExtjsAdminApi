package com.yinxing.webapi.code.controller.chart;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yinxing.framework.domain.R;
import com.yinxing.webapi.code.viewobje.sys.FusionchartVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/FusionchartsApi")
public class FusionchartsApi {

    @GetMapping("/doughnut2d")
    public R doughnut2d() {
        JSONObject root = new JSONObject();
        JSONObject chart = new JSONObject();
        chart.put("caption", "Android Distribution for our app");
        chart.put("subcaption", "For all users in 2017");
        chart.put("showpercentvalues", "1");
        chart.put("defaultcenterlabel", "Android Distribution");
        chart.put("aligncaptionwithcanvas", "0");
        chart.put("captionpadding", "0");
        chart.put("decimals", "1");
        chart.put("plottooltext", "<b>$percentValue</b> of our Android users are on <b>$label</b>");
        chart.put("centerlabel", "# Users: $value");
        chart.put("theme", "fusion");
        root.put("chart", chart);

        JSONArray data = new JSONArray();
        data.add(new FusionchartVo("Sandwich", "1000"));
        data.add(new FusionchartVo("Jelly Bean", "5300"));
        data.add(new FusionchartVo("Kitkat", "10500"));
        data.add(new FusionchartVo("Lollipop", "18900"));
        data.add(new FusionchartVo("Marshmallow", "17904"));

        root.put("data", data);
        return R.ok(root);
    }

    @GetMapping("/line")
    public R line() {
        JSONObject root = new JSONObject();
        JSONObject chart = new JSONObject();
        chart.put("caption", "Average Fastball Velocity");
        chart.put("yaxisname", "Velocity (in mph)");
        chart.put("subcaption", "[2005-2016]");
        chart.put("numbersuffix", " mph");
        chart.put("rotatelabels", "1");
        chart.put("setadaptiveymin", "1");
        chart.put("theme", "fusion");
        root.put("chart", chart);

        JSONArray data = new JSONArray();
        data.add(new FusionchartVo("2005", "89.45"));
        data.add(new FusionchartVo("2006", "89.87"));
        data.add(new FusionchartVo("2007", "89.64"));
        data.add(new FusionchartVo("2008", "90.13"));
        data.add(new FusionchartVo("2009", "90.67"));
        data.add(new FusionchartVo("2010", "90.54"));
        data.add(new FusionchartVo("2011", "90.75"));
        data.add(new FusionchartVo("2012", "90.8"));
        data.add(new FusionchartVo("2013", "91.16"));
        data.add(new FusionchartVo("2014", "91.37"));
        data.add(new FusionchartVo("2015", "91.66"));
        data.add(new FusionchartVo("2016", "91.8"));

        root.put("data", data);
        return R.ok(root);
    }
}
