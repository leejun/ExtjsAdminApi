package com.yinxing.webapi.code.viewobje.sys;

import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@ToString
public class RasswordResetVo {

    @NotBlank(message = "旧密码不能为空")
    @Length(min = 6, max = 20, message = "旧密长度必须6-20位")
    private String password;

    @NotBlank(message = "新密码不能为空")
    @Length(min = 6, max = 20, message = "新密码长度必须6-20位")
    private String passwordNew1;

    @NotBlank(message = "新密码不能为空")
    @Length(min = 6, max = 20, message = "新密码长度必须6-20位")
    private String passwordNew2;
}
