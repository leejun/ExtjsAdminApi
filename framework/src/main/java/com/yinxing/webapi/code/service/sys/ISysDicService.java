package com.yinxing.webapi.code.service.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yinxing.framework.mybatis.TemplateService;
import com.yinxing.webapi.code.entity.sys.SysDic;
import com.yinxing.webapi.code.viewobje.sys.DicTreeNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class ISysDicService extends TemplateService<SysDic> {

    /**
     * 查询字典树结构
     * @param sysDicTypeId sys_dictype表ID
     */
    public List<DicTreeNode> selectTreeList(long sysDicTypeId) {
        LambdaQueryWrapper<SysDic> qw = new LambdaQueryWrapper<>();
        qw.eq(SysDic::getSysDicTypeId_, sysDicTypeId);
        List<SysDic> dicList = baseMapper.selectList(qw);
        List<DicTreeNode> nodeList = sysDicToTreeNode(dicList);
        return convertToTreeList(nodeList);
    }

    /**
     * 查询字典树结构
     * @param sysDicTypeCode sys_dictype表Code_
     */
    public List<DicTreeNode> selectTreeList(String sysDicTypeCode) {
        List<SysDic> dicList = this.selectListByCode(sysDicTypeCode);
        List<DicTreeNode> nodeList = sysDicToTreeNode(dicList);
        return convertToTreeList(nodeList);
    }

    /**
     * 查询字典列表
     * @param sysDicTypeCode sys_dictype表Code_
     */
    public List<SysDic> selectListByCode(String sysDicTypeCode) {
        LambdaQueryWrapper<SysDic> qw = new LambdaQueryWrapper<>();
        qw.eq(SysDic::getSysDicTypeCode_, sysDicTypeCode);
        List<SysDic> dicList = baseMapper.selectList(qw);
        return dicList;
    }

    /**
     * 查询字典树结构
     * @param sysDicTypeCode sys_dictype表Code_
     */
    public List<DicTreeNode> selectTreeListNoStop(String sysDicTypeCode) {
        List<SysDic> dicList = this.selectListByCodeNoStop(sysDicTypeCode);
        List<DicTreeNode> nodeList = sysDicToTreeNode(dicList);
        return convertToTreeList(nodeList);
    }

    /**
     * 转换为EXT所需格式
     * @param dicList 字典列表集合
     */
    public List<DicTreeNode> sysDicToTreeNode(List<SysDic> dicList) {
        return dicList.stream().map(dic -> {
            DicTreeNode node = new DicTreeNode();
            node.setId(dic.getId_());
            node.setText(dic.getName_());
            node.setCode(dic.getCode_());
            node.setSysDicTypeId(dic.getSysDicTypeId_());
            node.setSysDicTypeCode(dic.getSysDicTypeCode_());
            node.setParentId(dic.getParentId_());
            node.setIndex(dic.getIndex_());
            node.setLeaf(dic.getLeaf_());
            node.setStop(dic.getStop_());
            node.setExpanded(dic.getExpanded_());
            node.setExpandedEdit(dic.getExpanded_());
            return node;
        }).collect(Collectors.toList());
    }



    /**
     * 查询字典列表
     * @param sysDicTypeCode sys_dictype表Code_
     */
    public List<SysDic> selectListByCodeNoStop(String sysDicTypeCode) {
        LambdaQueryWrapper<SysDic> qw = new LambdaQueryWrapper<>();
        qw.eq(SysDic::getSysDicTypeCode_, sysDicTypeCode);
        qw.eq(SysDic::getStop_, false);
        qw.orderByAsc(SysDic::getIndex_);
        List<SysDic> dicList = baseMapper.selectList(qw);
        return dicList;
    }

    /**
     * 把列表转换为树结构
     * @param nodeList 列表
     * @return 树结构的list
     */
    public List<DicTreeNode> convertToTreeList(List<DicTreeNode> nodeList) {
        List<DicTreeNode> rootList = new ArrayList<>();

        Map<Long, DicTreeNode> entityMap = new HashMap<>(128);
        for (DicTreeNode node: nodeList) {
            entityMap.put(node.getId(), node);
        }

        for (DicTreeNode node : nodeList){
            DicTreeNode parentEntity = entityMap.get(node.getParentId());
            if (parentEntity == null){
                rootList.add(node);
                continue;
            }
            parentEntity.getChildren().add(node);
        }

        return rootList;
    }

    /**
     * 批量新增|更新
     * @param records ext提交的数据格式
     */
    public void insertOrUpdateBatch(List<DicTreeNode> records) {
        for (DicTreeNode record: records) {
            log.debug(record.toString());
            SysDic sysDic = baseMapper.selectById(record.getId());
            //不是ext的根节点才处理
            if(record.getId() != 0) {
                if(sysDic != null) {
                    copyValue(sysDic, record);
                    baseMapper.updateById(sysDic);
                } else {
                    sysDic = new SysDic();
                    copyValue(sysDic, record);
                    baseMapper.insert(sysDic);
                }
            }
        }
    }

    /**
     * 拷贝属性
     * @param sysDic 字典数据
     * @param record ext提交的数据格式
     */
    private void copyValue(SysDic sysDic, DicTreeNode record) {
        sysDic.setId_(record.getId());
        sysDic.setParentId_(record.getParentId());
        sysDic.setLeaf_(record.isLeaf());
        sysDic.setName_(record.getText());
        sysDic.setCode_(record.getCode());
        sysDic.setSysDicTypeId_(record.getSysDicTypeId());
        sysDic.setSysDicTypeCode_(record.getSysDicTypeCode());
        sysDic.setIndex_(record.getIndex());
        sysDic.setStop_(record.isStop());
        sysDic.setExpanded_(record.isExpandedEdit());
    }

    /**
     * 批量删除
     * @param records ext提交的数据格式
     */
    public void deleteBatch(List<DicTreeNode> records) {
        for (DicTreeNode record: records) {
            baseMapper.deleteById(record.getId());
        }
    }

    /**
     * 启用|禁用
     * @param id 主键ID
     */
    public void stopById(long id) {
        SysDic record = baseMapper.selectById(id);
        if(record != null) {
            record.setStop_(!record.getStop_());
            this.updateById(record);
        }
    }

}