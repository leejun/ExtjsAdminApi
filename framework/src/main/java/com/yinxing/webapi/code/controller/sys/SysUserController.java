package com.yinxing.webapi.code.controller.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yinxing.framework.annotation.Json;
import com.yinxing.framework.domain.R;
import com.yinxing.webapi.code.entity.sys.SysUser;
import com.yinxing.webapi.code.service.sys.ISysUserService;
import com.yinxing.webapi.code.viewobje.sys.SysUserVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/SysUserController")
public class SysUserController {

    @Autowired
    private ISysUserService service;

    /**
     * 分页查询
     * @param page 分页模型(url?page=1&limit=20)
     * @param params 查询参数 url?page=1&limit=20&key1=value1&key2=value2
     */
    @GetMapping("/selectPage")
    public R selectPage(Page<SysUser> page, @RequestParam Map<String,String> params) {
        IPage<SysUser> iPage = service.selectUserList(page, params);
        return R.ok(iPage);
    }

    /**
     * 查询(已绑定角色|未绑定角色)的用户数据
     * @param page 分页参数
     * @param params 请求参数 (参数必须包含bindRole和sysRoleId)
     */
    @GetMapping("/selectUserListByRoleId")
    public R selectUserListByRoleId(Page<SysUser> page, @RequestParam Map<String,Object> params) {
        IPage<SysUser> iPage = service.selectUserListByRoleId(page, params);
        return R.ok(iPage);
    }

    /**
     * 绑定|解绑 角色
     * @param SysRoleId_ 角色id
     * @param records [{"RoleBind_":true,"Id_":"751489621128515588"},
     *                {"RoleBind_":true,"Id_":"751248414968254468"},
     *                {"RoleBind_":false,"Id_":"751787623789498372"}]
     */
    @PostMapping("/updateRoleUsers")
    public R updateRoleUsers(@RequestParam long SysRoleId_, @Json List<SysUserVo> records) {
        service.updateRoleUsers(SysRoleId_, records);
        return R.ok();
    }

    /**
     * 主键查询
     * @param id 主键
     */
    @GetMapping("/selectById")
    public R selectById(Long id) throws Exception {
        SysUserVo record = service.selectById(id);
        return R.ok(record);
    }

    /**
     * 主键查询
     * @param id 主键
     */
    @GetMapping("/selectDetailById")
    public R selectDetailById(Long id) throws Exception {
        SysUserVo record = service.selectDetailById(id);
        return R.ok(record);
    }

    /**
     * 主键删除
     * @param id 主键
     */
    @PostMapping("/deleteById")
    public R deleteById(Long id) {
        service.deleteUserAndRoleById(id);
        return R.ok();
    }

    /**
     * 主键批量删除
     * @param ids 主键
     */
    @PostMapping("/deleteBatchById")
    public R deleteBatchById(Long[] ids) {
        service.deleteUserAndRoleByIds(ids);
        return R.ok();
    }

    /**
     * 新增
     * @param record 实体类
     */
    @PostMapping("/insert")
    public R insert(SysUserVo record, @RequestParam(value = "file", required = false)
            MultipartFile file) throws Exception {
        service.insert(record, file);
        return R.ok();
    }

    /**
     * 更新
     * @param record 实体类
     */
    @PostMapping("/update")
    public R update(SysUserVo record, @RequestParam(value = "file", required = false)
            MultipartFile file) throws Exception {
        service.updateById(record, file);
        return R.ok();
    }
}
