package com.yinxing.webapi.code.controller.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yinxing.framework.domain.R;
import com.yinxing.webapi.code.entity.sys.SysGenerator;
import com.yinxing.webapi.code.service.sys.ISysGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/SysGeneratorController")
public class SysGeneratorController {

    @Autowired
    private ISysGeneratorService service;

    /**
     * 分页查询
     * @param page 分页模型(url?page=1&limit=20)
     * @param params 查询参数 url?page=1&limit=20&key1=value1&key2=value2
     */
    @GetMapping("/selectPage")
    public R selectPage(Page<SysGenerator> page, @RequestParam Map<String,String> params) {
        QueryWrapper<SysGenerator> qw = new QueryWrapper<>();
        if(params.containsKey("p1")) {
            qw.eq("StringVal1_", params.get("p1"));
        }

        if(params.containsKey("p2")) {
            qw.eq("IntVal1_", params.get("p2"));
        }

        if(params.containsKey("treeNodeId")) {
            qw.eq("SysDicCityId_", params.get("treeNodeId"));
        }
        IPage<SysGenerator> iPage = service.selectPage(page, qw);
        return R.ok(iPage);
    }

    /**
     * 主键查询
     * @param id 主键
     */
    @GetMapping("/selectById")
    public R selectById(@RequestParam Long id) {
        SysGenerator record = service.selectById(id);
        return R.ok(record);
    }

    /**
     * 主键删除
     * @param id 主键
     */
    @PostMapping("/deleteById")
    public R deleteById(@RequestParam Long id) {
        service.deleteById(id);
        return R.ok();
    }

    /**
     * 主键批量删除
     * @param ids 主键
    */
    @PostMapping("/deleteBatchById")
    public R deleteBatchById(@RequestParam Long[] ids) {
        service.deleteBatchById(ids);
        return R.ok();
    }

    /**
     * 新增
     * @param record 实体类
     */
    @PostMapping("/insert")
    public R insert(SysGenerator record) {
        service.insert(record);
        return R.ok();
    }

    /**
     * 更新
     * @param record 实体类
     */
    @PostMapping("/update")
    public R update(SysGenerator record) {
        service.updateById(record);
        return R.ok();
    }

}
