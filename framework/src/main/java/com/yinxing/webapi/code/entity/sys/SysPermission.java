package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 
 * </p>
 *
 * @author yinxing
 * @since 2022-01-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_permission")
public class SysPermission implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId("Id_")
    private Long Id_;

    /**
     * Controller类名称
     */
    private String ControllerName_;

    /**
     * Controller方法名称
     */
    private String MethodName_;

    /**
     * 功能说明
     */
    private String ActionName_;

    /**
     * Http请求方式
     */
    private String HttpMethod_;

    /**
     * 方法参数类型
     */
    private String ParametersType_;

    /**
     * 请求路径
     */
    private String UrlPattern_;

    /**
     * 代码是否删除
     */
    private Boolean CodeIDelete_;

    /**
     * 是否检查登录
     */
    private Boolean CheckLogin_;

    /**
     * 是否检查权限
     */
    private Boolean CheckPermission_;

    /**
     * 是否记录日志
     */
    private Boolean RequireLog_;

    /**
     * 是否记录请求参数
     */
    private Boolean LogRequestParam_;

    /**
     * 是否记录返回值
     */
    private Boolean LogReturnValue_;

    /**
     * 最后同步时间
     */
    private LocalDateTime LastSyncTime_;

}
