package com.yinxing.webapi.code.viewobje.sys;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * ExtJs(Ext.data.NodeInterface)所需格式
 */
@Data
@ToString
public class MenuTreeNode {
    //extjs-tree组件需要固定字段
    private long id;
    private String text;
    private int index;
    private long parentId;
    private String iconCls;
    private String rowCls;
    private boolean leaf;
    private boolean expanded;
    private List<MenuTreeNode> children = new ArrayList<>();
    private String qtip;
    private Boolean checked;
    //自定义字段
    private String viewType;
    private boolean stop = false;
    private boolean expandedEdit = false;
}
