package com.yinxing.webapi.code.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxing.webapi.code.entity.sys.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhixin
 * @since 2022-01-11
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> selectUserMenuTreeList(@Param("sysUserId") long sysUserId);
}
