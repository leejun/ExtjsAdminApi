package com.yinxing.webapi.code.controller.sys;

import com.yinxing.framework.annotation.Json;
import com.yinxing.framework.domain.R;
import com.yinxing.webapi.code.service.sys.SysMenuService;
import com.yinxing.webapi.code.viewobje.sys.MenuTreeNode;
import com.yinxing.webapi.shiro.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/SysMenuController")
public class SysMenuController {

    @Autowired
    private SysMenuService sysMenuService;

    /**
     * 查询全部菜单列表树(包含禁用状态下的数据)
     * 用于ExtJS菜单管理模块
     */
    @GetMapping("/selectMenuTreeList")
    public R selectMenuTreeList() {
        List<MenuTreeNode> menuList = sysMenuService.selectMenuTreeList();
        return R.ok(menuList);
    }

    /**
     * 查询用户绑定的菜单列表树
     * 用于ExtJS左侧菜单
     */
    @GetMapping("/selectUserMenuTreeList")
    public R selectUserMenuTreeList() {
        long sysUserId = LoginUser.current().getUserId();
        List<MenuTreeNode> menuList = sysMenuService.selectUserMenuTreeList(sysUserId);
        return R.ok(menuList);
    }

    /**
     * 查询菜单列表树(根据角色ID设置菜单选中状态)
     * 用于角色配置菜单模块
     * @param sysRoleId 角色ID
     */
    @GetMapping("/selectRoleMenuTreeList")
    public R selectRoleMenuTreeList(@RequestParam Long sysRoleId) {
        List<MenuTreeNode> menuList = sysMenuService.selectRoleMenuTreeList(sysRoleId);
        return R.ok(menuList);
    }

    /**
     * 批量新增或修改
     * @param records 菜单数据JSON集合
     * [{"id":"747076742044323841","text":"首页看板","index":0,"parentId":"0",
     * "iconCls":"x-fa fa-desktop","rowCls":"nav-tree-badge nav-tree-badge-new",
     * "leaf":false,"expanded":true,"children":[],"qtip":"首页统计分析","checked":null,
     * "viewType":"admindashboard","stop":false,"expandedEdit":false}]
     */
    @PostMapping("/insertOrUpdateBatch")
    public R insertOrUpdateBatch(@Json List<MenuTreeNode> records) {
        sysMenuService.insertOrUpdateBatch(records);
        return R.ok();
    }

    /**
     * 绑定|解绑 角色与菜单
     * @param sysRoleId 角色ID
     * @param records 菜单绑定与解绑数据集
     */
    @PostMapping("/updateRoleMenuBindInfo")
    public R updateRoleMenuBindInfo(@RequestParam Long sysRoleId, @Json List<MenuTreeNode> records) {
        sysMenuService.updateRoleMenuBindInfo(sysRoleId, records);
        return R.ok();
    }

    /**
     * 批量删除菜单
     * @param records 菜单数据JSON集合
     */
    @PostMapping("/deleteBatch")
    public R deleteBatch(@Json List<MenuTreeNode> records) {
        sysMenuService.deleteBatch(records);
        return R.ok();
    }

    /**
     * 启用|禁用
     * @param id 菜单ID
     */
    @PostMapping("/stopById")
    public R stopById(long id) {
        sysMenuService.stopById(id);
        return R.ok();
    }
}
