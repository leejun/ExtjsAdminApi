package com.yinxing.webapi.code.entity.sys;

import lombok.Data;
import lombok.ToString;


@Data
@ToString
public class SysMessageQuery extends SysMessage {

    private String SysDicName_;
    private String SysDicCode_;

}
