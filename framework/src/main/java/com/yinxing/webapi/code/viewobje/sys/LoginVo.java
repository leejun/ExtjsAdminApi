package com.yinxing.webapi.code.viewobje.sys;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
public class LoginVo {

    @NotBlank(message = "账号不能为空")
    @Length(min = 6, max = 20, message = "账户长度必须6-20位")
    private String username;

    @NotBlank(message = "密码不能为空")
    @Length(min = 6, max = 20, message = "密码长度必须6-20位")
    private String password;

    private String rememberMe;
}
