package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 此类所有字段全部手动改成public
 * 因为lombok的原因生成的get方法freemaker拿不到值
 * </p>
 *
 * @author yinxing
 * @since 2022-01-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_table")
public class SysTable implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId("Id_")
    public Long Id_;

    /**
     * 表名称
     */
    public String TableName_;

    /**
     * 表说明
     */
    public String TableComment_;

    /**
     * 表创建时间
     */
    public LocalDateTime CreateTime_;

    /**
     * 字段同步时间
     */
    public LocalDateTime SyncTime_;

    /**
     * 视图包名
     */
    public String Package_;

    /**
     * Ext布局类型
     */
    public String LayoutType_;

    /**
     * Model名称
     */
    public String ModelName_;

    /**
     * 字典类型ID
     */
    public Long SysDicTypeId_;

    /**
     * 字典类型编码
     */
    public String SysDicTypeCode_;

    /**
     * 表格选择方式(多选单选不选)
     */
    public String SelectModel_;

    /**
     * 表格是否分页
     */
    public Boolean IsPagination_;

    /**
     * 表格是否显示序号
     */
    public Boolean isRowNum_;

}
