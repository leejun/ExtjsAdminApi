package com.yinxing.webapi.code.service.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yinxing.framework.mybatis.TemplateService;
import com.yinxing.webapi.code.entity.sys.SysTable;
import com.yinxing.webapi.code.entity.sys.SysTableColumn;
import com.yinxing.webapi.code.mapper.sys.SysTableColumnMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class ISysTableColumnService extends TemplateService<SysTableColumn> {

    @Autowired
    private ISysTableService sysTableService;

    private SysTableColumnMapper getMapper() {
        return (SysTableColumnMapper) baseMapper;
    }

    /**
     * 查询表中字段集合
     * @param tableId 表ID
     */
    public List<SysTableColumn> selectListByTableId(long tableId) {
        LambdaQueryWrapper<SysTableColumn> qw = new LambdaQueryWrapper<>();
        qw.eq(SysTableColumn::getSysTableId_, tableId);
        List<SysTableColumn> list = baseMapper.selectList(qw);
        //第一次自动同步,以后手动同步.
        if(list == null || list.isEmpty()) {
            this.syncColumnList(tableId);
            list = baseMapper.selectList(qw);
        }
        return list;
    }

    /**
     * 把数据中查询到的表字段集合同步到我们自己的表中
     * @param tableId sys_table表ID
     */
    public void syncColumnList(long tableId) {
        SysTable sysTable = sysTableService.selectById(tableId);
        if(sysTable != null) {
            //更新同步时间
            sysTable.setSyncTime_(LocalDateTime.now());
            sysTableService.updateById(sysTable);

            //查询数据库表中所有字段,同步到我们的表.
            List<Map<String,Object>> list = getMapper().selectColumnList(sysTable.getTableName_());
            for (Map<String,Object> col : list) {
                String columnName = col.get("COLUMN_NAME").toString();
                String ordinalPosition = col.get("ORDINAL_POSITION").toString();
                String columnType = col.get("COLUMN_TYPE").toString();
                String columnComment = col.get("COLUMN_COMMENT").toString();
                SysTableColumn column = selectOne(tableId, columnName);
                if(column == null) {
                    column = new SysTableColumn();
                    column.setSysTableId_(tableId);
                    column.setName_(columnName);
                    column.setDataType_(columnType);
                    column.setComment_(columnComment);
                    column.setPosition_(Integer.parseInt(ordinalPosition));
                    column.setIsInsert_(true);
                    column.setIsUpdate_(true);
                    column.setIsList_(true);
                    column.setIsQuery_(false);
                    column.setIsQueryAction_(false);
                    column.setIsRequired_(false);
                    column.setIsDetail_(true);
                    column.setXtype_("textfield");
                    column.setSysDicTypeCode_("");
                    baseMapper.insert(column);
                }
            }
        }
    }

    /**
     * 查询列数据
     * @param tableId 主表ID
     * @param columnName 列名称
     */
    public SysTableColumn selectOne(long tableId, String columnName) {
        LambdaQueryWrapper<SysTableColumn> qw = new LambdaQueryWrapper<>();
        qw.eq(SysTableColumn::getSysTableId_, tableId);
        qw.eq(SysTableColumn::getName_, columnName);
        return baseMapper.selectOne(qw);
    }

    /**
     * 批量更新
     * @param records
     */
    public void updateBatch(List<SysTableColumn> records) {
        for (SysTableColumn column: records) {
            baseMapper.updateById(column);
        }
    }

    /**
     * 根据主表ID删除子集
     * @param sysTableId 主表ID
     */
    public void deleteBySysTableId(long sysTableId) {
        LambdaQueryWrapper<SysTableColumn> qw = new LambdaQueryWrapper<>();
        qw.eq(SysTableColumn::getSysTableId_, sysTableId);
        super.delete(qw);
    }

}