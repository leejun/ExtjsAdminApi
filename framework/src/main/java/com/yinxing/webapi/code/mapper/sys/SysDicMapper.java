package com.yinxing.webapi.code.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxing.webapi.code.entity.sys.SysDic;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yinxing
 * @since 2022-01-18
 */
public interface SysDicMapper extends BaseMapper<SysDic> {

}
