package com.yinxing.webapi.code.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxing.webapi.code.entity.sys.SysTableColumn;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yinxing
 * @since 2022-01-16
 */
public interface SysTableColumnMapper extends BaseMapper<SysTableColumn> {

    List<Map<String,Object>> selectColumnList(String tableName);
}
