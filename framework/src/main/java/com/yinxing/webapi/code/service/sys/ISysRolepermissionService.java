package com.yinxing.webapi.code.service.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yinxing.framework.mybatis.TemplateService;
import com.yinxing.webapi.code.entity.sys.SysRolepermission;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
public class ISysRolepermissionService extends TemplateService<SysRolepermission> {

    /**
     * 根据角色ID删除全部数据
     * @param sysRoleId 角色ID
     */
    public void deleteByRoleId(long sysRoleId) {
        LambdaQueryWrapper<SysRolepermission> qw = new LambdaQueryWrapper<>();
        qw.eq(SysRolepermission::getSysRoleId_, sysRoleId);
        super.delete(qw);
    }

    /**
     * 根据权限ID删除全部数据
     * @param sysPermissionId 权限ID
     */
    public void deleteByPermission(long sysPermissionId) {
        LambdaQueryWrapper<SysRolepermission> qw = new LambdaQueryWrapper<>();
        qw.eq(SysRolepermission::getSysPermissionId_, sysPermissionId);
        super.delete(qw);
    }

    /**
     * 根据权限ID删除全部数据
     * @param sysPermissionId 权限ID
     */
    public SysRolepermission selectRolePermission(long sysRoleId, long sysPermissionId) {
        LambdaQueryWrapper<SysRolepermission> qw = new LambdaQueryWrapper<>();
        qw.eq(SysRolepermission::getSysRoleId_, sysRoleId);
        qw.eq(SysRolepermission::getSysPermissionId_, sysPermissionId);
        return super.selectOne(qw);
    }

    /**
     * 解绑角色和权限
     * @param sysRoleId 角色ID
     * @param sysPermissionId 权限ID
     */
    public void unBindRolePermission(long sysRoleId, long sysPermissionId) {
        LambdaQueryWrapper<SysRolepermission> qw = new LambdaQueryWrapper<>();
        qw.eq(SysRolepermission::getSysRoleId_, sysRoleId);
        qw.eq(SysRolepermission::getSysPermissionId_, sysPermissionId);
        delete(qw);
    }

    /**
     * 绑定角色和权限
     * @param sysRoleId 角色ID
     * @param sysPermissionId 权限ID
     */
    public void bindRolePermission(long sysRoleId, long sysPermissionId) {
        //先查询一下，防止重复插入.
        if(selectRolePermission(sysRoleId, sysPermissionId) == null) {
            SysRolepermission sp = new SysRolepermission();
            sp.setSysRoleId_(sysRoleId);
            sp.setSysPermissionId_(sysPermissionId);
            super.insert(sp);
        }
    }
}