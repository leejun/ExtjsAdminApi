package com.yinxing.webapi.code.entity.sys;

import lombok.Data;

@Data
public class SysLogDetail extends SysLog {

    /**
     * 是否异常
     */
    private String ExceptionStr_;

    /**
     * 执行时间毫秒
     */
    private String RunTimeStr_;
}
