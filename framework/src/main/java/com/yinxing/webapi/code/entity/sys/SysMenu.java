package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 系统菜单表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_menu")
public class SysMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId("Id_")
    private Long Id_;

    /**
     * 菜单名称
     */
    @TableField("Name_")
    private String Name_;

    /**
     * 菜单顺序
     */
    @TableField("Index_")
    private Integer Index_;

    /**
     * 父菜单ID
     */
    @TableField("ParentId_")
    private Long ParentId_;

    /**
     * 是否叶子节点
     */
    @TableField("Leaf_")
    private Boolean Leaf_;

    /**
     * 菜单图标
     */
    @TableField("IconCls_")
    private String IconCls_;

    /**
     * 菜单图标
     */
    @TableField("RowCls_")
    private String RowCls_;

    /**
     * 主视图名称
     */
    @TableField("ViewType_")
    private String ViewType_;

    /**
     * 菜单是否展开
     */
    @TableField("Expanded_")
    private Boolean Expanded_;

    /**
     * 菜单鼠标悬停文本
     */
    @TableField("Qtip_")
    private String Qtip_;

    /**
     * 是否停用
     */
    @TableField("Stop_")
    private Boolean Stop_;
}
