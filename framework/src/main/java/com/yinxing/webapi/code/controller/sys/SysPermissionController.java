package com.yinxing.webapi.code.controller.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yinxing.framework.annotation.Json;
import com.yinxing.framework.domain.R;
import com.yinxing.webapi.code.entity.sys.SysPermission;
import com.yinxing.webapi.code.entity.sys.SysPermissionQuery;
import com.yinxing.webapi.code.service.sys.ISysPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/SysPermissionController")
public class SysPermissionController {

    @Autowired
    private ISysPermissionService service;

    /**
     * 分页查询
     * @param page 分页模型(url?page=1&limit=20)
     * @param params 查询参数 url?page=1&limit=20&key1=value1&key2=value2
     */
    @GetMapping("/selectPage")
    public R selectPage(Page<SysPermission> page, @RequestParam Map<String,String> params) {
        LambdaQueryWrapper<SysPermission> qw = new LambdaQueryWrapper<>();

        //条件查询
        final String queryParam = "QueryParam_";
        if(params.containsKey(queryParam)) {
            String queryValue = params.get(queryParam);
            qw.eq(SysPermission::getUrlPattern_, queryValue)
                    .or().eq(SysPermission::getControllerName_, queryValue)
                    .or().eq(SysPermission::getMethodName_, queryValue)
                    .or().likeRight(SysPermission::getActionName_, queryValue);
        }

        if(params.containsKey("CheckLogin_")) {
            qw.or().eq(SysPermission::getCheckLogin_, true);
        }
        if(params.containsKey("CheckPermission_")) {
            qw.or().eq(SysPermission::getCheckPermission_, true);
        }
        if(params.containsKey("RequireLog_")) {
            qw.or().eq(SysPermission::getRequireLog_, true);
        }

        qw.orderByAsc(SysPermission::getUrlPattern_);
        IPage<SysPermission> iPage = service.selectPage(page, qw);
        return R.ok(iPage);
    }

    /**
     * 查询需要授权的数据列表
     * @param params 查询条件 (参数必须包含bindRole="true"|"false"和 sysRoleId)
     *               注意"true"和"false"必须为字符,因为mybatis动态if按字符串处理的.
     */
    @GetMapping("/selectPermissionList")
    public R selectPermissionList(@RequestParam Map<String,Object> params) {
        List<SysPermissionQuery> list = service.selectPermissionList(params);
        return R.ok(list);
    }

    /**
     * 同步代码到数据库
     */
    @GetMapping("/codeSync")
    public R codeSync() {
        service.codeSync();
        return R.ok();
    }

    /**
     * 主键查询
     * @param id 主键
     */
    @GetMapping("/selectById")
    public R selectById(Long id) {
        SysPermission record = service.selectById(id);
        return R.ok(record);
    }

    /**
     * 主键删除
     * @param id 主键
     */
    @PostMapping("/deleteById")
    public R deleteById(Long id) {
        service.deleteMeAndAllPermById(id);
        return R.ok();
    }

    /**
     * 主键批量删除
     * @param ids 主键
    */
    @PostMapping("/deleteBatchById")
    public R deleteBatchById(Long[] ids) {
        service.deleteMeAndAllPermById(ids);
        return R.ok();
    }

    /**
     * 更新
     * @param records extjs提交的数据格式
     *  records: [{"ActionName_":"修改密码","CheckLogin_":true,"CheckPermission_":true,"RequireLog_":true,"Id_":"753341227046408269"},
     *  {"CheckLogin_":true,"CheckPermission_":true,"RequireLog_":true,"LogRequestParam_":true,"LogReturnValue_":true,"Id_":"753341223003099200"}]
     */
    @PostMapping("/updateBatch")
    public R updateBatch(@Json List<SysPermission> records) {
        service.updateBatch(records);
        return R.ok();
    }

    /**
     * 角色(绑定|解绑)权限
     * @param records: [{"RoleBind_":true,"Id_":"753341205303136266"},{"RoleBind_":true,"Id_":"753341225532264520"}]
     */
    @PostMapping("/updateRolePermission")
    public R updateRolePermission(@RequestParam long SysRoleId_, @Json List<SysPermissionQuery> records) {
        service.updateRolePermission(SysRoleId_, records);
        return R.ok();
    }
}
