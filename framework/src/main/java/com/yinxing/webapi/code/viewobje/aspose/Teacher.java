package com.yinxing.webapi.code.viewobje.aspose;

import java.util.ArrayList;

public class Teacher extends Person {

    public Teacher(String name, int age, ArrayList<Person> students) {
        super(name, age);
        m_Students = students;
    }

    private ArrayList<Person> m_Students;

    public ArrayList<Person> getStudents() {
        return m_Students;
    }
}
