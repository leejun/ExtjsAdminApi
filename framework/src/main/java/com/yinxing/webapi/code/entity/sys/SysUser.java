package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author yinxing
 * @since 2022-01-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_user")
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId("Id_")
    private Long Id_;

    /**
     * 姓名
     */
    private String Name_;

    /**
     * 登录名
     */
    private String LoginName_;

    /**
     * 登录密码: MD5(明文+盐)
     */
    private String Password_;

    /**
     * 盐值(32随机字符串)
     */
    private String Salt_;

    /**
     * 邮箱
     */
    private String Email_;

    /**
     * 电话
     */
    private String Phone_;

    /**
     * 部门ID
     */
    private Long SysDeptId_;

    /**
     * 创建时间
     */
    private LocalDateTime CreateTime_;

    /**
     * 禁用
     */
    private Boolean Stop_;

    /**
     * 头像路径
     */
    private String PictureUrl_;

    /**
     * 显示顺序
     */
    private Integer Index_;

    //扩展字段 用于连表查询
    @TableField(exist = false)
    private String SysDeptName_;
    @TableField(exist = false)
    private String SysDeptCode_;
    @TableField(exist = false)
    private Boolean RoleBind_;

    public static transient final String DB_Id_ = "Id_";
    public static transient final String DB_Name_ = "Name_";
    public static transient final String DB_LoginName_ = "LoginName_";
    public static transient final String DB_Password_ = "Password_";
    public static transient final String DB_Salt_ = "Salt_";
    public static transient final String DB_Email_ = "Email_";
    public static transient final String DB_Phone_ = "Phone_";
    public static transient final String DB_SysDeptId_ = "SysDeptId_";
    public static transient final String DB_CreateTime_ = "CreateTime_";
    public static transient final String DB_Stop_ = "Stop_";
    public static transient final String DB_PictureUrl_ = "PictureUrl_";
    public static transient final String DB_Index_ = "Index_";

}
