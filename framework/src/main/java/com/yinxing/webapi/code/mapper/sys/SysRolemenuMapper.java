package com.yinxing.webapi.code.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxing.webapi.code.entity.sys.SysRolemenu;

/**
 * <p>
 * 角色与菜单关系表 Mapper 接口
 * </p>
 *
 * @author yinxing
 * @since 2022-01-26
 */
public interface SysRolemenuMapper extends BaseMapper<SysRolemenu> {

}
