package com.yinxing.webapi.code.controller.sys;

import com.yinxing.framework.annotation.Json;
import com.yinxing.framework.domain.R;
import com.yinxing.webapi.code.entity.sys.SysDic;
import com.yinxing.webapi.code.service.sys.ISysDicService;
import com.yinxing.webapi.code.viewobje.sys.DicTreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/SysDicController")
public class SysDicController {

    @Autowired
    private ISysDicService service;

    /**
     * 主键删除
     * @param id 主键
     */
    @PostMapping("/deleteById")
    public R deleteById(String id) {
        service.deleteById(id);
        return R.ok();
    }

    /**
     * 查询列表树
     * @param sysDicTypeId 字典类别Id(sys_dictype表Id_)
     */
    @GetMapping("/selectTreeList")
    public R selectTreeList(Long sysDicTypeId) {
        if(sysDicTypeId == null || sysDicTypeId == 0) {
            return R.ok(new ArrayList<>(0));
        }
        List<DicTreeNode> dicList = service.selectTreeList(sysDicTypeId);
        return R.ok(dicList);
    }

    /**
     * 查询列表树
     * @param sysDicTypeCode 字典类别编码(sys_dictype表Code_字段)
     */
    @GetMapping("/selectTreeListByCode")
    public R selectTreeListByCode(String sysDicTypeCode) {
        if(!StringUtils.hasLength(sysDicTypeCode)) {
            return R.ok(new ArrayList<>(0));
        }
        List<DicTreeNode> dicList = service.selectTreeList(sysDicTypeCode);
        return R.ok(dicList);
    }

    /**
     * 查询列表树(不包含Stop停用的数据)
     * @param sysDicTypeCode 字典类别编码(sys_dictype表Code_字段)
     */
    @GetMapping("/selectTreeListByCodeNoStop")
    public R selectTreeListByCodeNoStop(String sysDicTypeCode) {
        if(!StringUtils.hasLength(sysDicTypeCode)) {
            return R.ok(new ArrayList<>(0));
        }
        List<DicTreeNode> dicList = service.selectTreeListNoStop(sysDicTypeCode);
        return R.ok(dicList);
    }

    /**
     * 查询评级列表
     * @param sysDicTypeCode 字典类别编码(sys_dictype表Code_字段)
     */
    @GetMapping("/selectListByCodeNoStop")
    public R selectListByCodeNoStop(String sysDicTypeCode) {
        if(!StringUtils.hasLength(sysDicTypeCode)) {
            return R.ok(new ArrayList<>(0));
        }
        List<SysDic> dicList = service.selectListByCodeNoStop(sysDicTypeCode);
        return R.ok(dicList);
    }

    /**
     * 批量新增或修改
     * @param records 数据JSON集合
     */
    @PostMapping("/insertOrUpdateBatch")
    public R insertOrUpdateBatch(@Json List<DicTreeNode> records) {
        service.insertOrUpdateBatch(records);
        return R.ok();
    }

    /**
     * 批量删除
     * @param records 数据JSON集合
     */
    @PostMapping("/deleteBatch")
    public R deleteBatch(@Json List<DicTreeNode> records) {
        service.deleteBatch(records);
        return R.ok();
    }

    /**
     * 启用|禁用
     * @param id 主键ID
     */
    @PostMapping("/stopById")
    public R stopById(long id) {
        service.stopById(id);
        return R.ok();
    }
}
