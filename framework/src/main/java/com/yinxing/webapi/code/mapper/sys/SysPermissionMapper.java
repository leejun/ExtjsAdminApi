package com.yinxing.webapi.code.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxing.webapi.code.entity.sys.SysPermission;
import com.yinxing.webapi.code.entity.sys.SysPermissionQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yinxing
 * @since 2022-01-29
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    List<SysPermissionQuery> selectPermissionList(@Param("params") Map<String, Object> params);

    List<SysPermission> selectPermissionsBySysUserId(@Param("sysUserId") long sysUserId);
}
