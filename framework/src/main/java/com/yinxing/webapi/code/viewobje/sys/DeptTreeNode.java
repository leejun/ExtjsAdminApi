package com.yinxing.webapi.code.viewobje.sys;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * ExtJs(Ext.data.NodeInterface)所需格式
 */
@Data
@ToString
public class DeptTreeNode {
    private long id;
    private long parentId;
    private int index;
    private String text;
    private boolean leaf;
    private boolean expanded;
    private List<DeptTreeNode> children = new ArrayList<>(0);
    //自定义数据
    private String deptCode;
    private boolean stop = false;
    private boolean expandedEdit = false;
}
