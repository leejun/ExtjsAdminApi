package com.yinxing.webapi.code.controller.sys;

import com.yinxing.framework.annotation.Json;
import com.yinxing.framework.domain.R;
import com.yinxing.webapi.code.service.sys.ISysDeptService;
import com.yinxing.webapi.code.viewobje.sys.DeptTreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/SysDeptController")
public class SysDeptController {

    @Autowired
    private ISysDeptService service;

    /**
     * 查询全部列表树
     */
    @GetMapping("/selectTreeList")
    public R selectTreeList() {
        List<DeptTreeNode> menuList = service.selectTreeList();
        return R.ok(menuList);
    }

    /**
     * 查询列表树(非禁用)
     */
    @GetMapping("/selectTreeListNoStop")
    public R selectTreeListNoStop() {
        List<DeptTreeNode> deptList = service.selectTreeListNoStop();
        return R.ok(deptList);
    }

    /**
     * 批量新增或修改
     * @param records 数据JSON集合
     */
    @PostMapping("/insertOrUpdateBatch")
    public R insertOrUpdateBatch(@Json List<DeptTreeNode> records) {
        service.insertOrUpdateBatch(records);
        return R.ok();
    }

    /**
     * 批量删除
     * @param records 数据JSON集合
     */
    @PostMapping("/deleteBatch")
    public R deleteBatch(@Json List<DeptTreeNode> records) {
        service.deleteBatch(records);
        return R.ok();
    }

    /**
     * 主键删除
     * @param id 主键
     */
    @PostMapping("/deleteById")
    public R deleteById(String id) {
        service.deleteById(id);
        return R.ok();
    }

    /**
     * 启用|禁用
     * @param id 主键ID
     */
    @PostMapping("/stopById")
    public R stopById(long id) {
        service.stopById(id);
        return R.ok();
    }
}
