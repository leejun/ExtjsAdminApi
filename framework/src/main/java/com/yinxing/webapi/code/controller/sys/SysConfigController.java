package com.yinxing.webapi.code.controller.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yinxing.framework.domain.R;
import com.yinxing.webapi.code.entity.sys.SysConfig;
import com.yinxing.webapi.code.service.sys.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/SysConfigController")
public class SysConfigController {

    @Autowired
    private ISysConfigService service;

    /**
     * 分页查询
     * @param page 分页模型(url?page=1&limit=20)
     * @param params 查询参数 url?page=1&limit=20&key1=value1&key2=value2
     */
    @GetMapping("/selectPage")
    public R selectPage(Page<SysConfig> page, @RequestParam Map<String,String> params) {
        QueryWrapper<SysConfig> qw = new QueryWrapper<>();
        if(params.containsKey("queryParam")) {
            String queryValue = params.get("queryParam");
            qw.eq("ConfigKey_", queryValue);
        }
        IPage<SysConfig> iPage = service.selectPage(page, qw);
        return R.ok(iPage);
    }

    /**
     * 主键查询
     * @param id 主键
     */
    @GetMapping("/selectById")
    public R selectById(@RequestParam Long id) {
        SysConfig record = service.selectById(id);
        return R.ok(record);
    }

    /**
     * 主键删除
     * @param id 主键
     */
    @PostMapping("/deleteById")
    public R deleteById(@RequestParam Long id) {
        service.deleteById(id);
        return R.ok();
    }

    /**
     * 新增
     * @param record 实体类
     */
    @PostMapping("/insert")
    public R insert(SysConfig record) {
        service.insert(record);
        return R.ok();
    }

    /**
     * 更新
     * @param record 实体类
     */
    @PostMapping("/update")
    public R update(SysConfig record) {
        service.updateById(record);
        return R.ok();
    }

}
