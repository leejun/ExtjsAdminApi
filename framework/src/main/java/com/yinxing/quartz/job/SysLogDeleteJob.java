package com.yinxing.quartz.job;

import com.yinxing.webapi.code.service.sys.ISysLogService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

@Slf4j
public class SysLogDeleteJob extends QuartzJobBean {

    @Autowired
    private ISysLogService sysLogService;

    @Override
    protected void executeInternal(JobExecutionContext context) {
        int deleteCount = sysLogService.deleteHistoryData(15);
        log.info("自动删除操作日志历史数据的任务已运行.共删除数据: {} 行", deleteCount);
    }
}
