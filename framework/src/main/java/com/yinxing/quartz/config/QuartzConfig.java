package com.yinxing.quartz.config;

import com.yinxing.quartz.job.HelloJob;
import com.yinxing.quartz.job.SysLogDeleteJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzConfig {

    @Bean
    public JobDetail helloJob() {
        JobDetail jobDetail = JobBuilder.newJob(HelloJob.class)
                //任务唯一标识要指定
                .withIdentity("job1")
                .withDescription("测试任务")
                .storeDurably()
                .build();
        return jobDetail;
    }

    @Bean
    public Trigger trigger() {
        Trigger trigger = TriggerBuilder.newTrigger()
                //触发器唯一标识必须要指定，否则每次启动生成UUID标识，会重复创建任务。
                .withIdentity("trigger1")
                .forJob(helloJob())
                .withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
                .build();
        return trigger;
    }

    @Bean
    public JobDetail sysLogDeleteJob() {
        JobDetail jobDetail = JobBuilder.newJob(SysLogDeleteJob.class)
                .withIdentity("SysLogDeleteJob")
                .withDescription("自动删除历史操作日志")
                .storeDurably()
                .build();
        return jobDetail;
    }

    @Bean
    public Trigger sysLogDeleteJobtrigger() {
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("trigger3")
                .forJob(sysLogDeleteJob())
                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 1 * * ?"))
                .build();
        return trigger;
    }
}
