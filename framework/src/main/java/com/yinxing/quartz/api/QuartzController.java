package com.yinxing.quartz.api;

import com.yinxing.framework.domain.R;
import com.yinxing.webapi.code.viewobje.sys.QuartzJobsVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/QuartzController")
public class QuartzController {

    @Autowired
    private QuartzService quartzService;

    /**
     * 查询引擎中全部任务列表
     */
    @GetMapping("/getAllJobs")
    public R getAllJobs(String queryParam) {
        List<QuartzJobsVo> quartzJobsVOList = quartzService.getAllJobs();
        if(StringUtils.hasLength(queryParam)) {
            quartzJobsVOList = quartzJobsVOList
                    .stream()
                    .filter(vo -> vo.getJobDescription().contains(queryParam)
                            || vo.getJobName().equals(queryParam))
                    .collect(Collectors.toList());
        }
        return R.ok(quartzJobsVOList);
    }

    /**
     * 添加任务
     * @param jobName 任务名称
     * @param jobDescription 任务说明
     * @param cronExpression 执行周期表达式
     */
    @PostMapping("/addJob")
    public R addJob(String jobName, String jobDescription, String cronExpression) {
        quartzService.addJob(jobName, jobDescription, cronExpression);
        return R.ok();
    }

    /**
     * 暂停任务
     * @param jobName 任务名称
     * @param jobGroupName 任务组名称
     */
    @PostMapping("/pauseJob")
    public R pauseJob(String jobName, String jobGroupName) {
        quartzService.pauseJob(jobName, jobGroupName);
        return R.ok();
    }

    /**
     * 恢复任务
     * @param jobName 任务名称
     * @param jobGroupName 任务组名称
     */
    @PostMapping("/resumeJob")
    public R resumeJob(String jobName, String jobGroupName) {
        quartzService.resumeJob(jobName, jobGroupName);
        return R.ok();
    }

    /**
     * 删除任务
     * @param jobName 任务名称
     * @param jobGroupName 任务组名称
     */
    @PostMapping("/deleteJob")
    public R deleteJob(String jobName, String jobGroupName) {
        quartzService.deleteJob(jobName, jobGroupName);
        return R.ok();
    }

    /**
     * 立即运行任务
     * @param jobName 任务名称
     * @param jobGroupName 任务组名称
     */
    @PostMapping("/triggerJob")
    public R triggerJob(String jobName, String jobGroupName) {
        quartzService.triggerJob(jobName, jobGroupName);
        return R.ok();
    }

    /**
     * 更新时间表达式
     * @param triggerName 触发器名称
     * @param triggerGroupName 触发器组名称
     */
    @PostMapping("/rescheduleJob")
    public R rescheduleJob(String triggerName, String triggerGroupName, String cronExpression) {
        quartzService.rescheduleJob(triggerName, triggerGroupName, cronExpression);
        return R.ok();
    }
}
