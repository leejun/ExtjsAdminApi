package com.yinxing;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = {
    RedisAutoConfiguration.class, RedisRepositoriesAutoConfiguration.class})
@MapperScan("com.yinxing.webapi.code.mapper")
@EnableCaching
@EnableAsync
@EnableTransactionManagement(proxyTargetClass = true)
@EnableAspectJAutoProxy(exposeProxy=true, proxyTargetClass=true)
public class Application  {

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class).run(args);
    }

}
