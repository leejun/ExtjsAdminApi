package com.yinxing.framework.convert;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

public class JsonHttpMessageConvert extends MappingJackson2HttpMessageConverter {

	public JsonHttpMessageConvert(ObjectMapper objectMapper) {
		super(objectMapper);
	}

	@Override
	public boolean canWrite(Class<?> clazz, MediaType mediaType) {
		return (!byte[].class.equals(clazz))
				&& (!String.class.equals(clazz))
				&& super.canRead(clazz, mediaType);
	}
	
}
