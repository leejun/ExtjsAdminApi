package com.yinxing.framework.convert;

import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class StringToLocalDateConvert implements Converter<String, LocalDate> {
    @Override
    public LocalDate convert(String s) {
        if(StringUtils.hasLength(s)) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            return LocalDate.parse(s, formatter);
        }
        return null;
    }
}
