package com.yinxing.framework.system;

import lombok.Data;

/**
 * JVM相关信息
 */
@Data
public class Jvm {

    /**
     * jvm最大可申请
     */
    private String max;

    /**
     * jvm已申请内存总量
     */
    private String total;

    /**
     * jvm已使用内存
     */
    private String use;

    /**
     * jvm内存使用率
     */
    private String usedRate;

    /**
     * jvm剩余内存
     */
    private String free;

    /**
     * JDK版本
     */
    private String version;

    /**
     * JDK路径
     */
    private String home;

    /**
     * JVM启动时间
     */
    private String startTime;

    /**
     * JVM运行时间
     */
    private String runTime;
}
