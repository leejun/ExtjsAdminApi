package com.yinxing.framework.shiro.exception;

import org.apache.shiro.ShiroException;

public class RequiresPermissionException extends ShiroException {
    public RequiresPermissionException() {
        super();
    }

    public RequiresPermissionException(String message) {
        super(message);
    }


    public RequiresPermissionException(Throwable cause) {
        super(cause);
    }

    public RequiresPermissionException(String message, Throwable cause) {
        super(message, cause);
    }
}
