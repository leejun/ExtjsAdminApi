package com.yinxing.framework.shiro.interceptor;

import com.yinxing.framework.shiro.exception.RequiresUserException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.aop.UserAnnotationHandler;

import java.lang.annotation.Annotation;

public class MyUserAnnotationHandler extends UserAnnotationHandler {

    public void assertAuthorized(Annotation a) throws AuthorizationException {
        if (a instanceof RequiresUser && getSubject().getPrincipal() == null) {
            throw new RequiresUserException("未能获取到用户信息");
        }
    }
}
