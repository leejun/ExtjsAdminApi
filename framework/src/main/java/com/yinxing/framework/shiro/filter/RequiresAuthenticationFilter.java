package com.yinxing.framework.shiro.filter;

import com.yinxing.framework.shiro.exception.RequiresAuthenticationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.web.filter.authc.AuthenticationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 当前用户必须经过登录认证
 * 判断条件:subject.isAuthenticated() == true
 */
public class RequiresAuthenticationFilter extends AuthenticationFilter {

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) {
        throw new RequiresAuthenticationException("用户未登录");
    }

}
