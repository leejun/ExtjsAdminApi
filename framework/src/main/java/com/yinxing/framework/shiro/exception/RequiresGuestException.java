package com.yinxing.framework.shiro.exception;

import org.apache.shiro.ShiroException;

public class RequiresGuestException extends ShiroException {
    public RequiresGuestException() {
        super();
    }

    public RequiresGuestException(String message) {
        super(message);
    }


    public RequiresGuestException(Throwable cause) {
        super(cause);
    }

    public RequiresGuestException(String message, Throwable cause) {
        super(message, cause);
    }
}
