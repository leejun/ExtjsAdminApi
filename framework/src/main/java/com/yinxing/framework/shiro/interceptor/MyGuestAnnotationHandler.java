package com.yinxing.framework.shiro.interceptor;

import com.yinxing.framework.shiro.exception.RequiresGuestException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.RequiresGuest;
import org.apache.shiro.authz.aop.GuestAnnotationHandler;

import java.lang.annotation.Annotation;

public class MyGuestAnnotationHandler extends GuestAnnotationHandler {

    public void assertAuthorized(Annotation a) throws AuthorizationException {
        if (a instanceof RequiresGuest && getSubject().getPrincipal() != null) {
            throw new RequiresGuestException("账户非游客身份");
        }
    }
}
