package com.yinxing.framework.shiro.exception;

import org.apache.shiro.ShiroException;

public class RequiresUserException extends ShiroException {
    public RequiresUserException() {
        super();
    }

    public RequiresUserException(String message) {
        super(message);
    }


    public RequiresUserException(Throwable cause) {
        super(cause);
    }

    public RequiresUserException(String message, Throwable cause) {
        super(message, cause);
    }
}
