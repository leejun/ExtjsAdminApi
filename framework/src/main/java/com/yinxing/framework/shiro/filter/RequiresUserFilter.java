package com.yinxing.framework.shiro.filter;

import com.yinxing.framework.shiro.exception.RequiresUserException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.web.filter.authc.UserFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 客户端RememberMe会把当前账户加密记录到Cookie里
 * 在访问时会从中恢复当前账户，但当前账户并没有登录
 * subject.getPrincipal() != null;
 */
public class RequiresUserFilter extends UserFilter {

    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) {
        throw new RequiresUserException("未能获取到用户信息");
    }
}
