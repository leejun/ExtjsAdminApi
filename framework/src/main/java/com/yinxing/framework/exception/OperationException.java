package com.yinxing.framework.exception;

public class OperationException extends YinXingException {

    public OperationException() {
    }

    public OperationException(String message) {
        super(message);
    }

    public OperationException(Throwable cause) {
        super(cause);
    }

    public OperationException(String message, Throwable cause) {
        super(message, cause);
    }
}
