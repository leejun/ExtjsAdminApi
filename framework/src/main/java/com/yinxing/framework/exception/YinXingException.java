package com.yinxing.framework.exception;

/**
 * 银杏.框架系统异常
 * 其它业务异常继承此类
 */
public class YinXingException extends RuntimeException {

    public YinXingException() {
    }

    public YinXingException(String message) {
        super(message);
    }

    public YinXingException(Throwable cause) {
        super(cause);
    }

    public YinXingException(String message, Throwable cause) {
        super(message, cause);
    }
}
