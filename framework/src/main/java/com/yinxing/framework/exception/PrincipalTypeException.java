package com.yinxing.framework.exception;

/**
 * 登录主题类型不匹配
 */
public class PrincipalTypeException extends YinXingException {

    public PrincipalTypeException() {
    }

    public PrincipalTypeException(String message) {
        super(message);
    }

    public PrincipalTypeException(Throwable cause) {
        super(cause);
    }

    public PrincipalTypeException(String message, Throwable cause) {
        super(message, cause);
    }
}
