package com.yinxing.framework.mybatis;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 业务Service需要继承此类
 * 封装MybatisPlsu(增.删.改.查)等基础方法
 */
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
public abstract class TemplateService<T> {

    @Autowired
    protected BaseMapper<T> baseMapper;

    public boolean insert(T entity) {
        return SqlHelper.retBool(baseMapper.insert(entity));
    }

    public int delete(Wrapper<T> queryWrapper) {
        return baseMapper.delete(queryWrapper);
    }

    public boolean deleteById(Serializable id) {
        return SqlHelper.retBool(baseMapper.deleteById(id));
    }

    public void deleteBatchById(Serializable[] id) {
        for (int i = 0; i < id.length; i++) {
            this.deleteById(id[i]);
        }
    }

    public boolean updateById(T entity) {
        return SqlHelper.retBool(baseMapper.updateById(entity));
    }

    public int update(T entity, Wrapper<T> updateWrapper) {
        return baseMapper.update(entity, updateWrapper);
    }

    public T selectById(Serializable id) {
        return baseMapper.selectById(id);
    }

    public T selectOne(Wrapper<T> queryWrapper) {
        return baseMapper.selectOne(queryWrapper);
    }

    public IPage<T> selectPage(IPage<T> page, Wrapper<T> queryWrapper) {
        return baseMapper.selectPage(page, queryWrapper);
    }

    public List<T> selectList(Wrapper<T> queryWrapper) {
        return baseMapper.selectList(queryWrapper);
    }

    public List<T> selectBatchIds(Collection<? extends Serializable> idList) {
        return baseMapper.selectBatchIds(idList);
    }
}
