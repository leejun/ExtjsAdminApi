package com.yinxing.framework.mybatis;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.yinxing.framework.utils.LongIdUtils;

/**
 * MybatisPlus-ID生成器接口实现
 * 使用Long类型ID生成器(比UUID效率高)
 */
public class LongTypeIdGenerator implements IdentifierGenerator {
    @Override
    public Number nextId(Object entity) {
        /*如果分布式部署 则需要new LongIdUtils(long workerId, long datacenterId)传入不同参数*/
        return LongIdUtils.DEFAULT.nextId();
    }
}
