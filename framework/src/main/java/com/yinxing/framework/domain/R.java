package com.yinxing.framework.domain;

import com.yinxing.framework.enums.RCode;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;

@Data
@AllArgsConstructor
public class R<T extends Object> implements Serializable {

    private Boolean success;
    private T data;
    private String message;
    private RCode code;

    private transient static final String emptyStr = "操作成功";
    private transient static final Object emptyData = new HashMap<String, String>(1);

    public static R ok() {
        return new R(true, emptyData, emptyStr, RCode.SUCCESS);
    }

    public static R ok(Object data) {
        return new R(true, data, emptyStr, RCode.SUCCESS);
    }

    public static R err() {
        return new R(false, emptyData, emptyStr, RCode.ERROR);
    }

    public static R err(String message) {
        return new R(false, emptyData, message, RCode.ERROR);
    }

    public static R err(String message, RCode type) {
        return new R(false, emptyData, message, type);
    }
}
