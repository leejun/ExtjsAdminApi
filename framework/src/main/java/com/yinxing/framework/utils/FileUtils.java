package com.yinxing.framework.utils;

import java.util.UUID;

public final class FileUtils {

    /**
     * 获取文件类型
     * 例如: readme.txt, 返回: txt
     * @param fileName 文件名
     * @return 后缀（不含".")
     */
    public static String getFileType(String fileName) {
        int separatorIndex = fileName.lastIndexOf(".");
        if (separatorIndex < 0) {
            return "";
        }
        return fileName.substring(separatorIndex + 1).toLowerCase();
    }

    /**
     * 获取全局唯一文件名
     * 如: readme.txt, 返回: B9FEE3AE033D4FF4A95D1EE31C72BF6C.txt
     * @param fileName 原始文件名
     * @return 返回UUID开头的文件名(全局唯一)
     */
    public static String getUuidFileName(String fileName) {
        String fileType = getFileType(fileName);
        String uniqueFileName = UUID.randomUUID().toString().toUpperCase().replace("-", "") + "." + fileType;
        return uniqueFileName;
    }

    /**
     * 获取全局唯一文件名
     * 如: readme.txt, 返回: 690968107564535809.txt
     * @param fileName 原始文件名
     * @return 返回数字序列开头的文件名(全局唯一)
     */
    public static String getSequenceFileName(String fileName) {
        String fileType = getFileType(fileName);
        String uniqueFileName = LongIdUtils.DEFAULT.nextId() + "." + fileType;
        return uniqueFileName;
    }

}
