package com.yinxing.framework.utils;

import com.alibaba.fastjson.JSON;

public class JsonUtils {

    public static String toJsonMissError(Object data) {
        try {
            return JSON.toJSONString(data);
        } catch (Exception e) { }
        return "";
    }

    public static String toJson(Object data) {
        return JSON.toJSONString(data);
    }
}
