package com.yinxing.framework.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateSerializer  extends JsonSerializer<Date>  {

	private final static String shortPattern = "yyyy-MM-dd";
	
	@Override
	public void serialize(Date value, JsonGenerator jgen,
			SerializerProvider provider) throws IOException {
		SimpleDateFormat sdf = new SimpleDateFormat(shortPattern);
		String outValue = sdf.format(value);
		jgen.writeString(outValue);
	}

}
