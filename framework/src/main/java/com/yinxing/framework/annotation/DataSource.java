package com.yinxing.framework.annotation;

import com.yinxing.framework.enums.SourceType;

import java.lang.annotation.*;

/**
 * 自定义多数据源切换注解
 */
@Target({ ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DataSource
{
    /**
     * 切换数据源名称
     */
    SourceType value() default SourceType.master;
}
