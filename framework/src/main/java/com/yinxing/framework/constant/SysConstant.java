package com.yinxing.framework.constant;

public class SysConstant {

    /**
     * JWT-TOKEN http-head
     */
    public static final String JWT_TOKEN_HTTP_HEADER = "jwt-token";

    /**
     * WEB-SESSION http-head
     */
    public static final String AUTH_TOKEN_HTTP_HEADER = "auth-token";

    /**
     * 空字符串
     */
    public static final String EMPTY_STRING = "";

    /**
     * JWT-TOKEN 用户名
     */
    public static final String JWT_TOKEN_DATA_USERID = "userId";
}
