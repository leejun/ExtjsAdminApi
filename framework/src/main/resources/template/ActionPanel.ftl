Ext.define('Admin.view.${sysTable.Package_}.ActionPanel', {
	extend : 'Admin.base.BaseActionPanel',
	xtype : '${xtypePrefix}_actionpanel',

	initComponent : function() {
		var me = this;

		Ext.applyIf(me, {
			items: {
				xtype: 'toolbar',
				items: [
					<#if actionList?size gt 0> <#-- list字段不为空则生成查询代码 -->
					{
						xtype : 'basesearchform',
						reference : 'searchForm',
						items : [<#list actionList as column> <#-- 循环列生成文本框 -->
							{
								xtype: '${column.Xtype_}',
								name : '${column.Name_}',
							<#if column.Xtype_ == "sys_dictype_sysdictreepicker">
								sysDicTypeCode: '${column.SysDicTypeCode_}',
							</#if>
							<#if column.Xtype_ == "sys_dic_sysdiccombobox">
								sysDicTypeCode: '${column.SysDicTypeCode_}',
							</#if>
							<#if !column_has_next>
								margin : '0 0 0 0',
							</#if>
								fieldLabel : '${column.Comment_}'
							}<#if column_has_next>,</#if> <#-- 处理最后一个元素的, -->
								</#list>
						]
					}, {
						iconCls : 'x-fa fa-times-circle',
						ui : 'soft-blue',
						handler : 'clearSearch'
					}, {
						iconCls : 'x-fa fa-search',
						ui : 'soft-blue',
						handler : 'searchRecord'
					}, {
						iconCls : 'x-fa fa-search-plus',
						ui : 'soft-blue',
						handler : 'searchPlusRecord'
					},
					</#if>
					{
						xtype: 'tbfill'
					}, {
						xtype: 'addbutton',
						handler: 'onAdd'
					}, '-', {
						xtype: 'delbutton',
						handler: 'onDel'
					}
				]
		    }
		});

		me.callParent(arguments);
	}
});