Ext.define('Admin.view.${sysTable.Package_}.DetailFormWinController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.${xtypePrefix}_detailformwincontroller',

    /*根据id加载数据*/
    loadDataById: function (id) {
        var form = this.lookup("formpanel");
        form.load({
            url: ServerUrl + '/${sysTable.ModelName_}Controller/selectById',
            method: 'get',
            params: {id: id}
        });
    },

    /*关闭窗口*/
    onCancel: function() {
        this.getView().close();
    }
});
