Ext.define('Admin.view.${sysTable.Package_}.MainView', {
    extend: 'Ext.container.Container',
    xtype: '${xtypePrefix}_mainview',

    controller: '${xtypePrefix}_mainviewcontroller',

    layout : {
        align : 'stretch',
        type : 'hbox'
    },

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_dic_sysdictree',
                sysDicTypeCode: '${sysTable.SysDicTypeCode_}',
                title: '字典标题手动修改',
                reference: 'leftTreeGrid',
                margin: '0 5 0 0',
                width: 200
            }, {
                xtype: 'container',
                flex: 1,
                layout: {
                    align: 'stretch',
                    type: 'vbox'
                },
                items: [{
                    xtype: '${xtypePrefix}_actionpanel',
                    reference: 'actionpanel'
                }, {
                    xtype: '${xtypePrefix}_gridlist',
                    reference: 'gridlist',
                    flex: 1
                }]
            }]
        });

        me.callParent(arguments);
    }

});