Ext.define('Admin.view.${sysTable.Package_}.GridList', {
    extend : 'Admin.base.BaseGridPanel',
    xtype : '${xtypePrefix}_gridlist',

<#if sysTable.SelectModel_ == "single">
    singleSelect: true,
</#if>
<#if sysTable.SelectModel_ == "multi">
    multiSelect: true,
</#if>
<#if sysTable.IsPagination_ == true>
    pagination: true,
</#if>

    initComponent : function() {
        var me = this;
        Ext.applyIf(me, {
            store : Ext.create('Admin.store.${sysTable.Package_}.${sysTable.ModelName_}Store', {
                autoLoad : true
            }),
            columns : [
            <#if sysTable.isRowNum_ == true>
                {xtype: 'rownumberer'},
            </#if>
    <#list columnList as column>
        <#if column.IsList_>
            <#if column_index = 0>
                {text:'${column.Comment_}', dataIndex:'${column.Name_}', hidden:true},
            <#elseif column_index = 1>
                {text:'${column.Comment_}', dataIndex:'${column.Name_}', minWidth:100, flex : 1},
            <#else>
                {text:'${column.Comment_}', dataIndex:'${column.Name_}', width:100},
            </#if>
        </#if>
    </#list>
                {
                    xtype: 'actioncolumn',
                    dataIndex: 'bool',
                    text: '操作',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-pencil-alt',
                            tooltip: '编辑',
                            handler: 'onEdit'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-times',
                            tooltip: '删除',
                            handler: 'onDelete'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-file',
                            tooltip: '详细',
                            handler: 'onShow'
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});