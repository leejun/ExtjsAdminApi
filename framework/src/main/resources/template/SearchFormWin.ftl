Ext.define('Admin.view.${sysTable.Package_}.SearchFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: '${xtypePrefix}_searchformwin',

    /*视图控制器 定义事件*/
    controller: '${xtypePrefix}_searchformwincontroller',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'formpanel',
                items: [<#list searchlList as column>
                    {
                        xtype: '${column.Xtype_}',
                    <#if column.Xtype_ == "sys_dic_sysdictreepicker">
                        sysDicTypeCode: '${column.SysDicTypeCode_}',
                    </#if>
                    <#if column.Xtype_ == "sys_dic_sysdiccombobox">
                        sysDicTypeCode: '${column.SysDicTypeCode_}',
                    </#if>
                        name : '${column.Name_}',
                        fieldLabel : '${column.Comment_}'
                    }<#if column_has_next>,</#if>
                </#list>
                ]
            }]
        });
        me.callParent(arguments);
    },

    tools: [{
        type:'refresh',
        tooltip: '清空查询条件',
        handler: 'onClean'
    }],

    buttons: [{
        text: '查询',
        iconCls : 'x-fa fa-search-plus',
        ui: 'soft-blue',
        handler: 'onSearch'
    }, {
        text: '取消',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});