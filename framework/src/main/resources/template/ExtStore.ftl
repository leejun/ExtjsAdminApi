Ext.define('Admin.store.${sysTable.Package_}.${sysTable.ModelName_}Store', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.${sysTable.Package_}.${sysTable.ModelName_}Model',

    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: ServerUrl + '/${sysTable.ModelName_}Controller/selectPage',
        reader: {
            type: 'json',
            rootProperty: 'data.records',
            totalProperty: 'data.total',
            successProperty: 'success'
        }
    },
    autoDestroy: true
});