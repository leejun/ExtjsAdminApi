Ext.define('Admin.view.${sysTable.Package_}.DetailFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: '${xtypePrefix}_detailformwin',

    /*视图控制器 定义事件*/
    controller: '${xtypePrefix}_detailformwincontroller',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
            xtype: 'baseformpanel',
            reference: 'formpanel',
                items: [<#list detailList as column>
                    {
                        xtype: 'textfield',
                        name : '${column.Name_}',
                        readOnly: true,
                        fieldLabel : '${column.Comment_}'
                    }<#if column_has_next>,</#if>
                </#list>
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        text: '关闭',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});