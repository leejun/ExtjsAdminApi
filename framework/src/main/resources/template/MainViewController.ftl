Ext.define('Admin.view.${sysTable.Package_}.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.${xtypePrefix}_mainviewcontroller',

    listen: {
        component: {
            'basesearchform[reference=searchForm] > textfield': {
                //antionPanel查询条件控件键盘事件
                specialkey: 'specialkeyHandler'
            }<#if sysTable.LayoutType_ == "treegrid">,</#if>

            <#if sysTable.LayoutType_ == "treegrid">
            //监听左侧树点击事件
            'treepanel[reference=leftTreeGrid]': {
                itemclick: 'leftTreeItemclick'
            }
            </#if>
        },

        controller: {
            '${xtypePrefix}_insertformwincontroller': {
                //监听数据新增事件(刷新表格)
                recordAdded: 'onRecordAdded'
            },
            '${xtypePrefix}_editformwincontroller': {
                //监听数据编辑事件(刷新表格)
                recordUpdated: 'onRecordUpdated'
            },
            '${xtypePrefix}_searchformwincontroller': {
                //监听高级查询事件(高级查询)
                searchPlus: 'onSearchPlus'
            }
        }
    },

    /*高级查询条件*/
    searchPlusParams: {},

<#if sysTable.LayoutType_ == "treegrid">
    /*左侧树单击事件*/
    leftTreeItemclick: function (tree, record, item, index, e, eOpts) {
        this.searchRecord();
    },
</#if>

    /*actionpanel-新增事件*/
    onAdd: function () {
        var me = this;
        var win = Ext.create({
            xtype: '${xtypePrefix}_insertformwin',
            title: '新增'
        });
        me.getView().add(win).show();
    },

    /*actionpanel-批量删除事件*/
    onDel: function () {
        var me = this;
        if (!me.lookup('gridlist').isSelected()) {
            Ext.popup.Msg('提示信息', '请选择一行记录!');
            return;
        }
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: me.deleteBatchRequest,
            scope: me
        });
    },
    deleteBatchRequest: function (buttonId) {
        if (buttonId === "ok") {
            var me = this;
            var records = this.lookup('gridlist').getSelectedRecords(),
                ids = [];
            Ext.each(records, function (item) {
                ids.push(item.getId());
            });
            ids = ids.join(",");
            Ext.Ajax.request({
                url: ServerUrl + '/${sysTable.ModelName_}Controller/deleteBatchById',
                method: 'POST',
                successHint: true,
                maskContainer: me.getView(),
                params: {
                    ids: ids
                },
                success: function () {
                    me.refreshGridList();
                }
            });
        }
    },

    /*gridlist-编辑事件*/
    onEdit: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        var win = Ext.create({
            xtype: '${xtypePrefix}_editformwin',
            title: '编辑'
        });
        me.getView().add(win).show();
        win.getController().loadDataById(record.getId());
    },

    /*gridlist-详细事件*/
    onShow: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        var win = Ext.create({
            xtype: '${xtypePrefix}_detailformwin',
            title: '详细'
        });
        me.getView().add(win).show();
        win.getController().loadDataById(record.getId());
    },

    /*gridlist-删除事件*/
    onDelete: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            recordId: record.getId(),
            fn: me.deleteRequest,
            scope: me
        });
    },

    /*发送删除数据请求*/
    deleteRequest: function (buttonId, text, opt) {
        var me = this;
        if (buttonId === "ok") {
            Ext.Ajax.request({
                url: ServerUrl + '/${sysTable.ModelName_}Controller/deleteById',
                //请求方式
                method: 'POST',
                //操作成功提示
                successHint: true,
                //网络请求时遮罩当前视图
                maskContainer: me.getView(),
                //请求参数
                params: {id: opt.recordId},
                //成功回调
                success: function () {
                    me.refreshGridList();
                }
            });
        }
    },

    /*添加数据完成事件*/
    onRecordAdded: function () {
        this.refreshGridList();
    },

    /*编辑数据完成事件*/
    onRecordUpdated: function () {
        this.refreshGridList();
    },

    /*高级查询事件*/
    onSearchPlus: function (searchParams) {
        this.searchPlusParams = searchParams;
        this.searchRecord();
    },

    /*刷新表格*/
    refreshGridList: function () {
        this.lookup('gridlist').refresh();
    },

    /*ActionPanel查询条件控件回车事件*/
    specialkeyHandler: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.searchRecord();
        }
        if (e.getKey() === e.ESC) {
            field.reset();
        }
    },

    /*ActionPanel条件查询*/
    searchRecord: function () {
        var requestParams = this.lookup('searchForm').getValues(false, true);
<#if sysTable.LayoutType_ == "treegrid">
        //左侧树选中节点ID
        var treeNodeId = this.lookup('leftTreeGrid').getSelectedRecordId();
        if (treeNodeId && treeNodeId !== '0') {
            requestParams.treeNodeId = treeNodeId;
        }
</#if>
        Ext.apply(requestParams, this.searchPlusParams);
        this.lookup('gridlist').search(requestParams);
    },

    /*ActionPanel高级查询*/
    searchPlusRecord: function () {
        var me = this;
        var win = Ext.create({
            xtype: '${xtypePrefix}_searchformwin',
            title: '查询'
        });
        me.getView().add(win).show();
        win.getController().setFormData(me.searchPlusParams);
    },

    /*ActionPanel清空查询条件*/
    clearSearch: function () {
        this.lookup('searchForm').reset();
        this.searchPlusParams = {};
<#if sysTable.LayoutType_ == "treegrid">
        this.lookup('leftTreeGrid').deselectAll();
</#if>
        this.searchRecord();
    }
});
